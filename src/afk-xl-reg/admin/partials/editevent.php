<?php
    $context = new afkContext();
    $event = $context->getEvent($_GET['id']);
    $seatTypes = $context->getSeatTypes();
    $minx = 1;
    $miny = 1;
    foreach($event->EventSeats as $e){
        if ($e->positionx+1 > $minx) {
            $minx = $e->positionx+1;
        }
        if ($e->positiony+1 > $miny){
            $miny = $e->positiony+1;
        }
    }
?>
<div id="edit-event" class="wrap clearfix">
    <div class="clearfix">
        <fieldset class="pull-left" style="max-width:500px;">
            <h3>Edit Event - <?php echo $event->id; ?></h3>
            <form id="edit-event-form" autocomplete="off">
                <input type="hidden" name="action" value="update_event" />
                <input type="hidden" name="id" id="eventid" value="<?php echo $event->id; ?>" />
                <div>
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control" required value="<?php echo $event->name; ?>" />
                </div>
                <div>
                    <label for="active">Is Active</label>
                    <input type="checkbox" name="active" id="active" <?php echo ($event->active ? 'checked' : '');?> />
                </div>
                <div>
                    <label for="date">Date</label>
                    <input type="datetime-local" name="date" id="date" class="form-control" required value="<?php echo date_format(date_create($event->date), "Y-m-d\TH:i"); ?>" />
                </div>
                <div>
                    <label for="minraisedamount">Minimum Donations $ Raised*</label>
                    <input type="number" name="minraisedamount" id="minraisedamount" min="0" class="form-control" required value="<?php echo $event->minraisedamount ?>" />
                </div>
                <div>
                    <label for="bonusamount">Extra Seats Min Donations $ Raised*</label>
                    <input type="number" name="bonusamount" id="bonusamount" min="0" class="form-control" required value="<?php echo $event->bonusamount ?>" />
                </div>
                <div>
                    <label for="bonusamountseats"># of Extra Seats</label>
                    <input type="number" name="bonusamountseats" id="bonusamountseats" min="0" class="form-control" required  value="<?php echo $event->bonusamountseats ?>" />
                </div>
                <div>
                    <label for="text">Description</label>
                    <textarea name="text" id="text" rows="5" class="form-control" required><?php echo $event->text; ?></textarea>
                </div>
                <div>
                    <label for="text">Terms and Conditions</label>
                    <textarea name="terms" id="terms" rows="5" class="form-control" required><?php echo $event->terms; ?></textarea>
                </div>
                <div>
                    <label for="url">URL</label>
                    <input type="text" name="url" id="url" class="form-control" value="<?php echo $event->url; ?>" />
                </div>
                <div>
                    <label for="grid">Grid Dimensions</label>
                    <input type="number" min="<?php echo $minx; ?>" max="100" name="gridwidth" id="gridwidth" placeholder="W" style="width:80px;" required value="<?php echo $event->gridwidth; ?>" />
                    x
                    <input type="number" min="<?php echo $miny; ?>" max="100" name="gridheight" id="gridheight" placeholder="H" style="width:80px;" required value="<?php echo $event->gridheight; ?>" />
                </div>
                <div>
                    <label for="backgroundimage">Grid Background Image</label> 
                    <div class="image-preview-wrapper">
                        <img id="image-preview" src="<?php echo $event->backgroundimage; ?>" width="100" height="100" style="max-height: 100px; width: 100px;">
                    </div>
                    <input id="upload_image_button" type="button" class="button" value="Select Image" />
                    <button type="button" class="button" onclick="jQuery('#backgroundimage').val('');jQuery('#image-preview').attr('src', '');">Clear Image</button>
                    <input type="hidden" name="backgroundimage" id="backgroundimage" value="<?php echo $event->backgroundimage; ?>" />
                </div>
                <div>
                    <label for="adminemail">Admin Email</label>
                    <input type="text" name="adminemail" id="adminemail" class="form-control" required value="<?php echo $event->adminemail; ?>" />
                </div>
                <br />
                <button type="button" class="button button-primary" onclick="updateEvent();">Save</button>
                <button type="button" class="button" onclick="jQuery('#edit-event').remove();">Cancel</button>
            </form>
        </fieldset>
        <fieldset class="pull-left">
            <h3>Seating Grid Preview</h3>
             <p>Recommended Background Image Size: &nbsp; <span id="grid-recommend-size"></span></p>
             <p>Click "Edit Seats" above to add or edit seat types.</p>
            <div id="grid-preview"></div>
        </fieldset>
        <fieldset id="grid-cell-info-wrapper" class="pull-left" style="display:none">
            <h3>Seat Info</h3>
            <div id="grid-cell-info">

            </div>
        </fieldset>
    </div>
</div>
<script type="text/javascript">

    var jsonSeatTypes = <?php echo json_encode($seatTypes); ?>;
    var jsonEvent = <?php echo json_encode($event); ?>;

    jQuery(function(){
        buildGrid('#grid-preview', jsonEvent.gridwidth, jsonEvent.gridheight, jsonEvent.backgroundimage);
        jQuery('#grid-recommend-size').html(getRecommendedImageSize(jsonEvent.gridwidth, jsonEvent.gridheight));
        jQuery('.grid-cell').data('eventid', jsonEvent.id);

        colorGridSeats(jsonEvent);

        jQuery('.grid-cell').click(function(){
            $this = jQuery(this);
            jQuery('.grid-cell').removeClass('active');
            
            $info = jQuery('#grid-cell-info');
            $info.html('');
            if ($this.data('id') > 0) {
                $this.addClass('active');
                var data;
                jsonEvent.EventSeats.forEach(function(e){
                    if (e.id == $this.data('id')) {
                        data = e;
                    }
                });

                $info.append('<p><b>Seat Type:</b> ' + data.SeatType.name + '</p>');
                $info.append('<p><b>Seat #:</b> ' + data.seatnumber + '</p>');
                if (data.Person) {
                    $info.append('<p><b>Participant:</b> ' + data.Person.name + '</p>');
                    $info.append('<p><b>Email:</b> ' + data.Person.email + '</p>');
                    $info.append('<p><b>Extra-Life ID:</b> ' + data.Person.participantid + '</p>');
                }
                else {
                    $info.append('<p><b>Participant:</b> None - Open Seat</p>');
                }
                jQuery('#grid-cell-info-wrapper').show();
            }
        });

    });
</script>