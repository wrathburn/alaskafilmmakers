<?php
    $context = new afkContext();
    $seatTypes = $context->getSeatTypes();
?>
<fieldset>
    <h3>Add New Event Seat</h3>
    <form id="formseat" autocomplete="off">
        <input type="hidden" name="eventid" value="<?php echo $_GET['eventid']; ?>">
        <input type="hidden" name="positionx" value="<?php echo $_GET['positionx']; ?>">
        <input type="hidden" name="positiony" value="<?php echo $_GET['positiony']; ?>">
        <input type="hidden" name="action" value="add_event_seat">
        <div>
            <label for="seattypeid">Seat Type</label>
            <select id="seattypeid" name="seattypeid" class="form-control" required>
                <?php foreach($seatTypes as $st) { ?>
                    <option value="<?php echo $st->id; ?>" style="color:<?php echo $st->color; ?>"><?php echo $st->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div>
            <label for="seatnumber">Seat #</label>
            <input type="text" name="seatnumber" id="seatnumber" value="" required class="form-control">
        </div>
        <br>
        <div>
            <button type="submit" class="button button-primary">Add Seat</button>
            <button type="button" onclick="reloadEditEventSeats()" class="button">Cancel</button>
        </div>
    </form>
</fieldset>
<script type="text/javascript">
    jQuery('#seatnumber').focus();
    formseat.onsubmit = function(e){
        e.preventDefault();
        if (formseat.checkValidity()) {
            var data = {
                action: 'add_event_seat',
                eventid: formseat.elements.eventid.value,
                positionx: formseat.elements.positionx.value,
                positiony: formseat.elements.positiony.value,
                seattypeid: formseat.elements.seattypeid.value,
                seatnumber: formseat.elements.seatnumber.value
            };
            jQuery.post(ajaxurl, data, function(response){
                if (response.length > 0) {
                    alert(response);    
                }
                reloadEditEventSeats();
            });
        }
        else {
            alert('All fields are required.');
        }
    };

</script>