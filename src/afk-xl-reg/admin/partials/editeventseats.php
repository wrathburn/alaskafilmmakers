<?php
    $context = new afkContext();
    $event = $context->getEvent($_GET['id']);
    $seatTypes = $context->getSeatTypes();
    $minx = 1;
    $miny = 1;
    foreach($event->EventSeats as $e){
        if ($e->positionx+1 > $minx) {
            $minx = $e->positionx+1;
        }
        if ($e->positiony+1 > $miny){
            $miny = $e->positiony+1;
        }
    }
?>
<div id="edit-event" class="wrap clearfix">
    <div class="clearfix">
        <fieldset class="pull-left" style="max-width:33%;">
            <h3>Event Details- <?php echo $event->id; ?> <?php echo ($event->active ? 'Active' : 'Inactive') ?></h3>
            <table class="wp-list-table widefat striped pages">
                <tr>
                    <th>Name</th>
                    <td><?php echo $event->name; ?></td>
                </tr>
                <tr>
                    <th>Active</th>
                    <td><?php echo $event->active ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td><?php $date = date_create($event->date); echo date_format($date, "l jS F Y \@ g:ia"); ?></td>
                </tr>
                <tr>
                    <th>Minimun Donation $ Raised</th>
                    <td><?php echo '$'.number_format($event->minraisedamount,0) ?></td>
                </tr>
                <tr>
                    <th>Extra Seats Minimun Donation $ Raised</th>
                    <td><?php echo '$'.number_format($event->bonusamount,0) ?></td>
                </tr>
                <tr>
                    <th># of Extra Seats Allowed</th>
                    <td><?php echo $event->bonusamountseats ?></td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td><p><?php echo $event->text; ?></p></td>
                </tr>
                <tr>
                    <th>Terms and Conditions</th>
                    <td><p><?php echo $event->terms; ?></p></td>
                </tr>
                <tr>
                    <th>URL</th>
                    <td><?php echo $event->url; ?></td>
                </tr>
                <tr>
                    <th>Grid Dimensions</th>
                    <td><?php echo $event->gridwidth; ?> x <?php echo $event->gridheight; ?></td>
                </tr>
                <tr>
                    <th>Grid Background Image</th> 
                    <td>
                        <div class="image-preview-wrapper">
                            <img id="image-preview" src="<?php echo $event->backgroundimage; ?>" width="100" height="100" style="max-height: 100px; width: 100px;">
                        </div>
                    </td>
                    
                </tr>
                <tr>
                    <th>Admin Email</th>
                    <td><?php echo $event->adminemail ?></td>
                </tr>
            </table>
        </fieldset>
        <fieldset class="pull-left">
            <h3>Seating Grid Preview</h3>
             <p>Recommended Background Image Size: &nbsp; <span id="grid-recommend-size"></span></p>
             <p>Click on a cell to add or update an event seat.</p>
            <div id="grid-preview"></div>
        </fieldset>
        <div class="pull-left" id="edit-seat">
        
        </div>
    </div>
</div>
<script type="text/javascript">

    var jsonSeatTypes = <?php echo json_encode($seatTypes); ?>;
    var jsonEvent = <?php echo json_encode($event); ?>;

    function reloadEditEventSeats(){
        jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=editeventseats&id=' + jsonEvent.id);
    }

    jQuery(function(){
        buildGrid('#grid-preview', jsonEvent.gridwidth, jsonEvent.gridheight, jsonEvent.backgroundimage);
        jQuery('#grid-recommend-size').html(getRecommendedImageSize(jsonEvent.gridwidth, jsonEvent.gridheight));
        jQuery('.grid-cell').data('eventid', jsonEvent.id);
        colorGridSeats(jsonEvent);

        jQuery('.grid-cell').hover(function(){
            $this = jQuery(this);
            $info = jQuery('#grid-cell-info');
            $info.html('');
            if ($this.data('id') > 0) {
                var data;
                jsonEvent.EventSeats.forEach(function(e){
                    if (e.id == $this.data('id')) {
                        data = e;
                    }
                });

                $info.append('<p><b>Seat Type:</b> ' + data.SeatType.name + '</p>');
                if (data.Person) {
                    $info.append('<p><b>Participant:</b> ' + data.Person.name + '</p>');
                    $info.append('<p><b>Email:</b> ' + data.Person.email + '</p>');
                    $info.append('<p><b>Extra-Life ID:</b> ' + data.Person.participantid + '</p>');
                }
                else {
                    $info.append('<p><b>Participant:</b> None - Open Seat</p>');
                }
                jQuery('#grid-cell-info-wrapper').show();
            }
            
            
        }, function(){
            jQuery('#grid-cell-info-wrapper').hide();
        });

        jQuery('#grid-preview').on('click', '.grid-cell', function(e){
            var $this = jQuery(this);
            var data = {
                id: parseInt($this.data('id')),
                eventid: parseInt(jsonEvent.id),
                positionx: parseInt($this.data('positionx')),
                positiony: parseInt($this.data('positiony')),
                seattypeid: parseInt($this.data('seattypeid')),
                personid: parseInt($this.data('personid'))
            };

            if (data.id > 0) {
                // edit seat
                data.action = 'afk_get_partial';
                data.partial = 'editseat';
                jQuery.get(ajaxurl, data, function(response){
                    jQuery('#edit-seat').html(response);
                });
                
            } else {
                // add seat
                data.action = 'afk_get_partial';
                data.partial = 'addseat';
                jQuery.get(ajaxurl, data, function(response){
                    jQuery('#edit-seat').html(response);
                });
            }
        });

    });
</script>