<?php
	if (current_user_can('administrator') === false) {
		wp_die();
	}
    $context = new afkContext();
    $events = $context->getEvents();
    $seatTypes = $context->getSeatTypes();
?>
    <div class="clearfix">
        <fieldset class="wrap">
            <h3 class="clearfix" style="margin-top:0;">Extra Life Events
            <button type="button" class="button button-primary pull-right" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=addevent');">Add Event</button>
            </h3>
            <div id="table-events">
                <table class="wp-list-table widefat striped pages">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Shortcode</th>
                            <th>Active?</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Min $ Raised</th>
                            <th>Extra Seats Min $ Raised</th>
                            <th># of Extra Seats</th>
                            <th>Url</th>
                            <th>Grid</th>
                            <th>Grid Image</th>
                            <th>Admin Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($events as $e) { ?>
                        <tr>
                            <td><button type="button" class="button button-small" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=eventdetails&id=<?php echo $e->id; ?>');">Details</button></td>
                            <td>[xlevent id="<?php echo $e->id ?>"]</td>
                            <td><?php echo ($e->active ? 'Yes' : 'No') ?></td>
                            <td><?php echo $e->name ?></td>
                            <td><?php echo date_format(date_create($e->date), 'm-d-Y \@ g:ia'); ?></td>
                            <td><?php echo '$'.number_format($e->minraisedamount,0) ?></td>
                            <td><?php echo '$'.number_format($e->bonusamount,0) ?></td>
                            <td><?php echo $e->bonusamountseats ?></td>
                            <td><a href="<?php echo $e->url ?>" target="_blank">view</a></td>
                            <td><?php echo $e->gridwidth . ' x ' . $e->gridheight ?></td>
                            <td><a href="<?php echo $e->backgroundimage ?>" target="_blank" ><img src="<?php echo $e->backgroundimage ?>" style="height:20px;" /></a></td>
                            <td><?php echo $e->adminemail ?></td>
                            <td>
                                <button type="button" class="button button-small" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=editevent&id=<?php echo $e->id; ?>');">Edit</button>
                                <button type="button" class="button button-small" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=editeventseats&id=<?php echo $e->id; ?>');">Edit Seats</button>
                                <button type="button" class="button button-small" onclick="return deleteEvent(<?php echo $e->id ?>);">Delete</button>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <p>
                    Add a public registration form to any Page by including the shortcode for the event.  The registration form will only show if the event is active and before the date of the event.
                </p>
            </div>
            
        </fieldset>

        <fieldset class="wrap pull-left">
            <h3 class="clearfix" style="margin-top:0;">
                Seat Types
                <button type="button" class="button button-primary pull-right" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=addseattype');">
                    Add Seat Type
                </button>
            </h3>
            <div id="table-seat-types">
                <table class="wp-list-table widefat striped pages">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Color</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($seatTypes as $e) { ?>
                        <tr>
                            <td><?php echo $e->id ?></td>
                            <td><?php echo $e->name ?></td>
                            <td>
                                
                                <span style="display:inline-block;width:60px;height:20px;background-color:<?php echo $e->color ?>;">&nbsp;</span>
                                <?php echo $e->color ?>
                            </td>
                            <td>
                                <button type="button" class="button button-small" onclick="jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=editseattype&id=<?php echo $e->id; ?>');">Edit</button>
                                
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </fieldset>
        
    </div>
    
    <hr />
    <div id="afk-load"></div>
