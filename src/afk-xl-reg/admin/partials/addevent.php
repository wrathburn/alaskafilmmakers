
<div id="add-event" class="wrap clearfix">
    <div class="clearfix">
        <fieldset class="pull-left">
            <h3>Add Event</h3>
            <form id="add-event-form" autocomplete="off">
                <input type="hidden" name="action" value="add_event" />
                <div>
                    <label for="name">Name*</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div>
                    <label for="active">Is Active</label>
                    <input type="checkbox" name="active" id="active" checked="checked" />
                </div>
                <div>
                    <label for="date">Date*</label>
                    <input type="datetime-local" name="date" id="date" class="form-control" required />
                </div>
                <div>
                    <label for="minraisedamount">Minimum Donations $ Raised*</label>
                    <input type="number" name="minraisedamount" id="minraisedamount" min="0" class="form-control" required />
                </div>
                <div>
                    <label for="bonusamount">Extra Seats Min Donations $ Raised*</label>
                    <input type="number" name="bonusamount" id="bonusamount" min="0" class="form-control" required />
                </div>
                <div>
                    <label for="bonusamountseats"># of Extra Seats</label>
                    <input type="number" name="bonusamountseats" id="bonusamountseats" min="0" class="form-control" required />
                </div>
                <div>
                    <label for="text">Description* (Where)</label>
                    <textarea name="text" id="text" rows="5" class="form-control" required></textarea>
                </div>
                <div>
                    <label for="text">Terms and Conditions</label>
                    <textarea name="terms" id="terms" rows="5" class="form-control" required></textarea>
                </div>
                <div>
                    <label for="url">URL</label>
                    <input type="text" name="url" id="url" class="form-control" />
                </div>
                <div>
                    <label for="grid">Grid Dimensions*</label>
                    <input type="number" value="20" min="1" max="100" name="gridwidth" id="gridwidth" placeholder="W" class="form-control" style="width:80px;" required />
                    x
                    <input type="number" value="30" min="1" max="100" name="gridheight" id="gridheight" placeholder="H" class="form-control" style="width:80px;" required />
                </div>
                <div>
                    <label for="backgroundimage">Grid Background Image</label>
                    
                    <div class='image-preview-wrapper'>
                        <img id='image-preview' src='' width='100' height='100' style='max-height: 100px; width: 100px;'>
                    </div>
                    <input id="upload_image_button" type="button" class="button" value="Select Image" />
                    
                    <input type="hidden" name="backgroundimage" id="backgroundimage" class="form-control" />
                </div>
                <div>
                    <label for="adminemail">Admin Email*</label>
                    <input type="text" name="adminemail" id="adminemail" class="form-control" required />
                </div>
                <br />
                <button type="button" class="button button-primary" onclick="addEvent();">Save</button>
                <button type="button" class="button" onclick="jQuery('#add-event').remove();">Cancel</button>
            </form>
        </fieldset>
        <fieldset class="pull-left">
            <h3>Seating Grid Preview</h3>
            <p>Recommended Background Image Size: &nbsp; <span id="grid-recommend-size"></span></p>
            <div id="grid-preview"></div>
        </fieldset>
    </div>
    <script type="text/javascript">
    jQuery(function(){
        buildGrid('#grid-preview', jQuery('#gridwidth').val(), jQuery('#gridheight').val(), jQuery('#backgroundimage').val());
        jQuery('#grid-recommend-size').html(getRecommendedImageSize(jQuery('#gridwidth').val(), jQuery('#gridheight').val()));

        jQuery('body').on('change', '#gridwidth, #gridheight, #backgroundimage', function() {
			buildGrid('#grid-preview', jQuery('#gridwidth').val(), jQuery('#gridheight').val(), jQuery('#backgroundimage').val());
			jQuery('#grid-recommend-size').html(getRecommendedImageSize(jQuery('#gridwidth').val(), jQuery('#gridheight').val()));
		});
    });
    </script>
</div>
