<?php 
    $context = new afkContext();
    $seattype = $context->getSeatType($_GET['id']);
    if ($seattype === false){
        echo 'Invalid id';
    }
    else {
?>

<div id="edit-seat-type" class="wrap">
    <div class="clearfix">
        <fieldset class="pull-left">
            <h3>Edit Seat Type</h3>
            <form id="edit-seat-type-form" autocomplete="off">
                <input type="hidden" name="action" value="add_seat_type" />
                <input type="hidden" name="id" id="seattypeid" value="<?php echo $seattype->id ?>" />
                <div>
                    <label for="name">Name*</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?php echo $seattype->name ?>" required />
                </div>
                <div>
                    <label for="color">Color*</label>
                    <input type="color" name="color" id="color" value="<?php echo $seattype->color ?>" required style="height:40px;" />
                </div>
                <br />
                <button type="button" class="button button-primary" onclick="updateSeatType();">Save</button>
                <button type="button" class="button" onclick="jQuery('#edit-seat-type').remove();">Cancel</button>
            </form>
        </fieldset>
    </div>
</div>
<?php } ?>