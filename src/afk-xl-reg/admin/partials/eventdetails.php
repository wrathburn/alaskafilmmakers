<?php
    $context = new afkContext();
    $event = $context->getEvent($_GET['id']);
    $seatTypes = $context->getSeatTypes();
?>
<div id="edit-event" class="wrap clearfix">
    <div class="clearfix">
        <div class="pull-left"  style="max-width:33%;">
            <fieldset>
                <h3>Event Details - <?php echo $event->id; ?>  <?php echo ($event->active ? 'Active' : 'Inactive') ?></h3>
                <table class="wp-list-table widefat striped pages">
                    <tr>
                        <th>Name</th>
                        <td><?php echo $event->name; ?></td>
                    </tr>
                    <tr>
                        <th>Active</th>
                        <td><?php echo $event->active ? 'Yes' : 'No'; ?></td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td><?php $date = date_create($event->date); echo date_format($date, "l jS F Y \@ g:ia"); ?></td>
                    </tr>
                    <tr>
                        <th>Minimun Donation $ Raised</th>
                        <td><?php echo '$'.number_format($event->minraisedamount,0) ?></td>
                    </tr>
                    <tr>
                        <th>Extra Seats Minimun Donation $ Raised</th>
                        <td><?php echo '$'.number_format($event->bonusamount,0) ?></td>
                    </tr>
                    <tr>
                        <th># of Extra Seats Allowed</th>
                        <td><?php echo $event->bonusamountseats ?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td><p><?php echo $event->text; ?></p></td>
                    </tr>
                    <tr>
                        <th>Terms and Conditions</th>
                        <td><p><?php echo $event->terms; ?></p></td>
                    </tr>
                    <tr>
                        <th>URL</th>
                        <td><?php echo $event->url; ?></td>
                    </tr>
                    <tr>
                        <th>Grid Dimensions</th>
                        <td><?php echo $event->gridwidth; ?> x <?php echo $event->gridheight; ?></td>
                    </tr>
                    <tr>
                        <th>Grid Background Image</th> 
                        <td>
                            <div class="image-preview-wrapper">
                                <img id="image-preview" src="<?php echo $event->backgroundimage; ?>" width="100" height="100" style="max-height: 100px; width: 100px;">
                            </div>
                        </td>
                        
                    </tr>
                    <tr>
                        <th>Admin Email</th>
                        <td><?php echo $event->adminemail ?></td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <h3>Event Participants</h3>
                <table class="wp-list-table widefat striped pages">
                    <thead>
                        <tr>
                            <th>Seat#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Extra Life Id</th>
                            <th>Specs</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($event->EventSeats as $s) { 
                            if ($s->personid > 0){
                                $p = $s->Person;
                            
                        ?>
                        <tr>
                            <td><?php echo $s->seatnumber; ?></td>

                            <td><?php echo $p->name; ?></td>
                            <td><?php echo $p->email; ?></td>
                            <td><a target="_blank" href="https://www.extra-life.org/index.cfm?fuseaction=donordrive.participant&participantID=<?php echo $p->participantid; ?>"><?php echo $p->participantid; ?></a> </td>
                            <td><?php echo $p->specs; ?></td>
                            <td></td>
                        </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </fieldset>
        </div>
        
        <fieldset class="pull-left">
            <h3>Seating Grid Preview</h3>
             <p>Click on a cell to view details.</p>
            <div id="grid-preview"></div>
        </fieldset>
        <fieldset id="grid-cell-info-wrapper" class="pull-left" style="display:none">
            <h3>Seat Info</h3>
            <div id="grid-cell-info">

            </div>
            <br />
            <button id="add-person" type="button" class="button button-primary" style="display:none;" onclick="loadAddPerson(jQuery(this).data('id'));">
                Add Person
            </button>
            <hr />
            <div id="add-person-form"></div>
        </fieldset>
    </div>
    <br />
    <div class="clearfix">
        
        
    </div>
</div>
<script type="text/javascript">
    var jsonSeatTypes = <?php echo json_encode($seatTypes); ?>;
    var jsonEvent = <?php echo json_encode($event); ?>;

    function reloadEventDetails(){
        jQuery('#afk-load').load(ajaxurl + '?action=afk_get_partial&partial=eventdetails&id=' + jsonEvent.id);
    }

    function loadAddPerson(id){
        if (id > 0) {
            var data = {
                action: 'afk_get_partial',
                partial: 'addpersontoseat',
                id: id
            };
            jQuery.get(ajaxurl, data, function(html){
                jQuery('#add-person-form').html(html);
            });
        }
    }

    function removePersonFromSeat(id){
        if (id > 0 && confirm('Are you sure you want to remove this person from their seat? An email will be sent to notify the person.')) {
            var data = {
                action: 'remove_person_from_event_seat',
                eventseatid: id
            };

            jQuery.post(ajaxurl, data, function(html){
                if (html.length > 0)
                    alert(html);
                reloadEventDetails();
            }).fail(function(){
                alert('Whoops an error occured. Page will refresh and you can try again.')
                window.location.reload(true);
            });
        }
        else{
            alert('No seat id. Please click on the seat again.');
        }
    }

    jQuery(function(){
        buildGrid('#grid-preview', jsonEvent.gridwidth, jsonEvent.gridheight, jsonEvent.backgroundimage);
        jQuery('#grid-recommend-size').html(getRecommendedImageSize(jsonEvent.gridwidth, jsonEvent.gridheight));
        jQuery('.grid-cell').data('eventid', jsonEvent.id);

        colorGridSeats(jsonEvent);

        

        jQuery('#grid-preview').on('click', '.grid-cell', function(){
            jQuery('.grid-cell').removeClass('active');
            jQuery('#add-person').hide().data('id', 0);
            jQuery('#add-person-form').html('');
            $this = jQuery(this);
            $info = jQuery('#grid-cell-info');
            $info.html('');
            if ($this.data('id') > 0) {
                $this.addClass('active');
                var data;
                jsonEvent.EventSeats.forEach(function(e){
                    if (e.id == $this.data('id')) {
                        data = e;
                    }
                });

                $info.append('<p><b>Seat Type:</b> ' + data.SeatType.name + '</p>');
                $info.append('<p><b>Seat #:</b> ' + data.seatnumber + '</p>');
                if (data.Person) {
                    $info.append('<p><b>Participant:</b> ' + data.Person.name + '</p>');
                    $info.append('<p><b>Email:</b> ' + data.Person.email + '</p>');
                    $info.append('<p><b>Extra Life ID:</b>  <a href="https://www.extra-life.org/index.cfm?fuseaction=donordrive.participant&participantID=' + data.Person.participantid + '" target="_blank">' + data.Person.participantid + '</a></p>');
                    $info.append('<p><b>Specs/Notes:</b></p><pre>' + data.Person.specs + '</pre>');
                    $info.append('<br /><button class="button" onclick="removePersonFromSeat(' + data.id + ')">Remove Person From Seat</button>')
                }
                else {
                    $info.append('<p><b>Participant:</b> None - Open Seat</p>');
                    jQuery('#add-person').show();
                    jQuery('#add-person').data('id', data.id);
                }
                jQuery('#grid-cell-info-wrapper').show();
            }
        });

    });
</script>