
<div id="add-seat-type" class="wrap">
    <div class="clearfix">
        <fieldset class="pull-left">
            <h3>Add Seat Type</h3>
            <form id="add-seat-type-form" autocomplete="off">
                <input type="hidden" name="action" value="add_seat_type" />
                <div>
                    <label for="name">Name*</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div>
                    <label for="color">Color*</label>
                    <input type="color" name="color" id="color" required style="height:40px;" />
                </div>
                <br />
                <button type="button" class="button button-primary" onclick="addSeatType();">Save &amp; Add Seat Type</button>
                <button type="button" class="button" onclick="jQuery('#add-seat-type').remove();">Cancel</button>
            </form>
        </fieldset>
    </div>
</div>