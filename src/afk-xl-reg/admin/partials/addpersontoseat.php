<?php 
    $context = new afkContext();
    $person = new afkPerson();
    $id = $_GET['id'];
    $seat = $context->getEventSeat($id);
    if (!is_null($seat) && is_null($seat->personid)) {
?>
<br />
    <form id="addpersonform" name="addpersonform" autocomplete="off">
        <input type="hidden" id="eventseatid" name="eventseatid" value="<?php echo $id; ?>">
        <div>
            <label for="participantID">Extra-Life Participant ID</label>
            <input type="text" name="participantid" id="participantid" value="" class="form-control" required />
        </div>
        <p>
            Total Raised Amount <b>$<span id="total-raised"></span></b>
        </p>
        <div>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="" readonly class="form-control" required />
        </div>
        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="" class="form-control" required />
        </div>
        <div>
            <label for="specs">Equipment Specs and Notes</label>
            <textarea id="specs" name="specs" rows="3" class="form-control"></textarea>
        </div>
        <br />
        <button type="button" class="button button-primary" disabled onclick="addPersonToSeat()">Save and Add Person To Seat</button>
    </form>
    <script type="text/javascript">
        jQuery(function(){
            jQuery('#participantid').on('change', function(){
                jQuery('button', '#addpersonform').hide();
                getExtraLifeJSON(jQuery(this).val(), getExtraLifeJSONCallback)
            });
        });
        
        function getExtraLifeJSONCallback(data) {
            if (data != null){
                jQuery('#name', '#addpersonform').val(data.displayName);
                jQuery('#total-raised').html(data.sumDonations);
                jQuery('button', '#addpersonform').show();
                jQuery('button', '#addpersonform').prop('disabled', false);
            } else {
                alert('Invalid extra life participant id');
                addpersonform.reset();
                jQuery('button', '#addpersonform').prop('disabled', true);
            }
        }

        function addPersonToSeat(){
           
            var form = jQuery('#addpersonform')[0];
            if (form.checkValidity()){
                var url = ajaxurl;
                var data = {
                    action: 'add_person_to_event_seat',
                    eventseatid: form.elements.eventseatid.value,
                    name: form.elements.name.value,
                    email: form.elements.email.value,
                    participantid: form.elements.participantid.value,
                    specs: form.elements.specs.value
                };

                jQuery.post(url, data, function(data){
                    if (data){
                        alert(data);
                    }
                    reloadEventDetails();
                });
            }
        }
    </script>
    <?php
}
else {
    ?>
    <i>The Event Seat ID was not passed or this seat is already assigned.  Please click on a seat and try again.</i>
    <?php
}
?>