<?php
    $context = new afkContext();
    $seatTypes = $context->getSeatTypes();
    $seat = $context->getEventSeat($_GET['id']);
?>
<fieldset>
    <h3>Edit Event Seat</h3>
    <form id="formseat" autocomplete="off">
        <input type="hidden" name="id" value="<?php echo $seat->id; ?>">
        <?php if ($seat->personid > 0) {  ?>
            <div>
                <div style="color:red;border:1px solid red;background:#fff;padding:7.5px;">
                    Warning! This seat already has a person registered.<br> Please notify the registrant of any seat type or seat number changes.
                </div>
                <p><b>Name:</b>  <?php echo $seat->Person->name; ?></p>
                <p><b>Email:</b>  <?php echo $seat->Person->email; ?></p>
                <p><b>ID:</b>  <?php echo $seat->Person->participantid; ?></p>
            </div>
        <?php } ?>
        <div>
            <label for="seattypeid">Seat Type</label>
            <select id="seattypeid" name="seattypeid" class="form-control" required>
                <?php foreach($seatTypes as $st) { ?>
                    <option value="<?php echo $st->id; ?>" style="color:<?php echo $st->color; ?>" <?php echo ($st->id == $seat->seattypeid ? 'selected' : ''); ?>><?php echo $st->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div>
            <label for="seatnumber">Seat #</label>
            <input type="text" name="seatnumber" id="seatnumber" value="<?php echo $seat->seatnumber; ?>" required class="form-control">
        </div>
        <br>
        <div class="clearfix">
            <button type="submit"  class="button button-primary">Save</button>
            <button type="button" onclick="reloadEditEventSeats()" class="button">Cancel</button>
            
        </div>

        <?php if (is_null($seat->personid)) { ?>
        <div style="text-align:right">
            <hr />
            <button type="button" onclick="deleteEventSeat()" class="button button-small">Delete</button>
            <script type="text/javascript">
                 function deleteEventSeat(data) {
                    if (confirm('Are you sure you want to delete this seat?')){
                        var data = {
                            action: "delete_event_seat",
                            id: formseat.elements.id.value
                        };
                        jQuery.post(ajaxurl, data, function(result){
                            if (result.length > 0)
                                alert(result);
                            reloadEditEventSeats();
                        })
                        .fail(function(){
                            alert('Sorry an error occurred');
                        });
                    }
                }
            </script>
        </div>
            
        <?php } ?>

    </form>
</fieldset>
<script type="text/javascript">
    formseat.onsubmit = function (e){
        e.preventDefault();
        if (formseat.checkValidity()) {
            var data = {
                action: 'update_event_seat',
                id: formseat.elements.id.value,
                seattypeid: formseat.elements.seattypeid.value,
                seatnumber: formseat.elements.seatnumber.value
            };
            jQuery.post(ajaxurl, data, function(response){
                if (response.length > 0) {
                    alert(response);    
                }
                reloadEditEventSeats();
            });
        }
        else {
            alert('All fields are required.');
        }
    }

   
</script>