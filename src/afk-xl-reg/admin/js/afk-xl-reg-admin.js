function buildGrid(e, w, h, img) {
    e = jQuery(e);
    w = parseInt(w);
    h = parseInt(h);

    e.html('').addClass('grid')
        .width((w * 25))
        .height((h * 25))
        .css('background-image', 'url(' + img + ')');

    for (var i = 0; i < h; i++) {
        var row = jQuery('<div class="grid-row clearfix"></div>')
        for (var j = 0; j < w; j++) {
            row.append('<div class="grid-cell" data-positiony="' + i + '" data-positionx="' + j + '"></div>');
        }
        e.append(row);
    }
}

function colorGridSeats(json) {
    json.EventSeats.forEach(function(e) {
        var cell = jQuery('.grid-cell[data-positionx="' + e.positionx + '"][data-positiony="' + e.positiony + '"]').first();
        cell.data('id', e.id).data('seattypeid', e.seattypeid).data('personid', e.personid);
        cell.attr('title', e.seatnumber).html(e.seatnumber);
        cell.css('background-color', e.SeatType.color);
        if (e.personid > 0) {
            cell.addClass('taken');
        }
    });
}

function getRecommendedImageSize(w, h) {
    w = parseInt(w);
    h = parseInt(h);
    return (w * 25) + ' x ' + (h * 25);
}

function addSeatType() {
    var $form = jQuery('#add-seat-type-form')[0];
    if ($form.checkValidity()) {
        var data = {
            action: 'add_seat_type',
            name: $form.elements.name.value,
            color: $form.elements.color.value
        };

        jQuery.post(ajaxurl, data, function(result) {
                alert(result);
                window.location.reload(true);
            })
            .fail(function() {
                alert('Sorry an error occurred');
            });
    } else { alert('Missing required fields'); }
}

function updateSeatType() {
    var $form = jQuery('#edit-seat-type-form')[0];
    if ($form.checkValidity()) {
        var data = {
            action: 'update_seat_type',
            id: $form.elements.id.value,
            name: $form.elements.name.value,
            color: $form.elements.color.value
        };

        jQuery.post(ajaxurl, data, function(result) {
                alert(result);
                window.location.reload(true);
            })
            .fail(function() {
                alert('Sorry an error occurred');
            });
    } else { alert('Missing required fields'); }
}

function addEvent() {

    var $form = jQuery('#add-event-form')[0];
    if ($form.checkValidity()) {
        var data = {
            action: 'add_event',
            name: $form.elements.name.value,
            date: $form.elements.date.value,
            text: $form.elements.text.value,
            url: $form.elements.url.value,
            gridwidth: $form.elements.gridwidth.value,
            gridheight: $form.elements.gridheight.value,
            backgroundimage: $form.elements.backgroundimage.value,
            adminemail: $form.elements.adminemail.value,
            active: $form.elements.active.checked ? 1 : 0,
            minraisedamount: $form.elements.minraisedamount.value,
            bonusamount: $form.elements.bonusamount.value,
            bonusamountseats: $form.elements.bonusamountseats.value,
            terms: $form.elements.terms.value
        };

        jQuery.post(ajaxurl, data, function(result) {
                alert(result);
                window.location.reload(true);
            })
            .fail(function() {
                alert('Sorry an error occurred');
            });
    } else { alert('Missing required fields'); }
}

function updateEvent() {
    var $form = jQuery('#edit-event-form')[0];
    if ($form.checkValidity()) {
        var data = {
            action: 'update_event',
            id: $form.elements.id.value,
            name: $form.elements.name.value,
            date: $form.elements.date.value,
            text: $form.elements.text.value,
            url: $form.elements.url.value,
            gridwidth: $form.elements.gridwidth.value,
            gridheight: $form.elements.gridheight.value,
            backgroundimage: $form.elements.backgroundimage.value,
            adminemail: $form.elements.adminemail.value,
            active: $form.elements.active.checked ? 1 : 0,
            minraisedamount: $form.elements.minraisedamount.value,
            bonusamount: $form.elements.bonusamount.value,
            bonusamountseats: $form.elements.bonusamountseats.value,
            terms: $form.elements.terms.value
        };

        jQuery.post(ajaxurl, data, function(result) {
                alert(result);
                window.location.reload(true);
            })
            .fail(function() {
                alert('Sorry an error occurred');
            });
    } else { alert('Missing required fields or invalid fields.'); }
}

function deleteEvent(id) {
    if (confirm('Are you sure?')) {
        var data = {
            action: 'delete_event',
            id: id
        };
        jQuery.post(ajaxurl, data, function(result) {
            alert(result);
            window.location.reload(true);
        }).fail(function() {
            alert('Sorry an error has occurred.');
            window.location.reload(true);
        });
    }
}

function getExtraLifeJSON(id, callback) {
    var api = 'https://www.extra-life.org/api/participants/' + id;

    jQuery.getJSON(api, callback).fail(function() {
        callback(null);
    });
}

(function($) {
    'use strict';

    $(function() {
        // Uploading files
        var file_frame;

        $('body').on('click', '#upload_image_button', function(event) {
            event.preventDefault();
            // Create the media frame.
            file_frame = wp.media.frames.file_frame = wp.media({
                title: 'Select a image to upload',
                button: {
                    text: 'Use this image',
                },
                multiple: false // Set to true to allow multiple files to be selected
            });

            // When an image is selected, run a callback.
            file_frame.on('select', function() {
                // We set multiple to false so only get one image from the uploader
                var attachment = file_frame.state().get('selection').first().toJSON();
                // Do something with attachment.id and/or attachment.url here
                $('#image-preview').attr('src', attachment.url).css('width', 'auto');
                $('#backgroundimage').val(attachment.url).trigger('change');
            });

            // Finally, open the modal
            file_frame.open();
        });


    });

})(jQuery);