<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/admin
 * @author     Your Name <email@example.com>
 */
class AFK_XL_REG_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $afk_xl_reg    The ID of this plugin.
	 */
	private $afk_xl_reg;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $afk_xl_reg       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $afk_xl_reg, $version ) {

		$this->afk_xl_reg = $afk_xl_reg;
		$this->version = $version;

	}

	public function create_menu() {
		add_menu_page('Extra Life', 'Extra Life Events', 'administrator', 'afk-admin-home',array($this,'admin_home'),'', null);
	}

	public function admin_home() {
		include 'partials/events.php';
	}
	
	public function get_partial() {
		if(isset($_GET['partial'])) {
			switch ($_GET['partial']) {
				case 'events':
					include 'partials/events.php';
					break;
				case 'addevent':
					include 'partials/addevent.php';
					break;
				case 'editevent':
					include 'partials/editevent.php';
					break;
				case 'editeventseats':
				include 'partials/editeventseats.php';
					break;
				case 'eventdetails':
					include 'partials/eventdetails.php';
					break;
				case 'addseattype':
					include 'partials/addseattype.php';
					break;
				case 'editseattype':
					include 'partials/editseattype.php';
					break;
				case 'addpersontoseat';
					include 'partials/addpersontoseat.php';
					break;
				case 'editseat';
					include 'partials/editseat.php';
					break;
				case 'addseat';
					include 'partials/addseat.php';
					break;
				default:
					wp_die('Nothing Found');
					break;
			}
			die();
		}
	}

	public function add_event() {

		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$event = new afkEvent();
			$event->name = $_POST['name'];
			$event->date = $_POST['date'];
			$event->text = $_POST['text'];
			$event->url = $_POST['url'];
			$event->gridwidth = $_POST['gridwidth'];
			$event->gridheight = $_POST['gridheight'];
			$event->backgroundimage = $_POST['backgroundimage'];
			$event->adminemail = $_POST['adminemail'];
			$event->active = $_POST['active'];
			$event->minraisedamount = $_POST['minraisedamount'];
			$event->bonusamount = $_POST['bonusamount'];
			$event->bonusamountseats = $_POST['bonusamountseats'];
			$event->terms = $_POST['terms'];

			$context = new afkContext();
			$result = $context->addEvent($event);
			if ($result === false){
				echo 'Failed to add event.  Please try again';
			}
			else {
				echo 'Event added!';
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function update_event() {
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$event = new afkEvent();
			$event->id = $_POST['id'];
			$event->name = $_POST['name'];
			$event->date = $_POST['date'];
			$event->text = $_POST['text'];
			$event->url = $_POST['url'];
			$event->gridwidth = $_POST['gridwidth'];
			$event->gridheight = $_POST['gridheight'];
			$event->backgroundimage = $_POST['backgroundimage'];
			$event->adminemail = $_POST['adminemail'];
			$event->active = $_POST['active'];
			$event->minraisedamount = $_POST['minraisedamount'];
			$event->bonusamount = $_POST['bonusamount'];
			$event->bonusamountseats = $_POST['bonusamountseats'];
			$event->terms = $_POST['terms'];

			$context = new afkContext();
			$result = $context->updateEvent($event);

			if ($result > 0){
				echo 'Event updated!';
				
			}
			else {
				echo 'Failed to update event.  Please try again';
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();

	}

	// AJAX POST
	public function delete_event() {
		
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$event = new afkEvent();
			$event->id = $_POST['id'];
			$context = new afkContext();
			$result = $context->deleteEvent($event);

			if ($result > 0){
				echo 'Event deleted!';
				
			}
			else {
				echo 'Failed to delete event.  Please try again.';
			}
		}
		else {
			echo 'Missing parameters for delete.';
		}

		wp_die();
	}

	public function add_seat_type() {
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$data = new afkSeatType();
			$data->name = $_POST['name'];
			$data->color = $_POST['color'];
			
			$context = new afkContext();
			$result = $context->addSeatType($data);
			if ($result === false){
				echo 'Failed to add seat type.  Please try again';
			}
			else {
				echo 'Seat Type added!';
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function update_seat_type() {
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$data = new afkSeatType();
			$data->id = $_POST['id'];
			$data->name = $_POST['name'];
			$data->color = $_POST['color'];
			
			$context = new afkContext();
			$result = $context->updateSeatType($data);
			if ($result === false){
				echo 'Failed to update seat type.  Please try again';
			}
			else {
				echo 'Seat Type updated!';
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();

	}

	public function update_event_seat() {
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$data = new afkEventSeat();
			
			$data->id = $_POST['id'];
			$data->seattypeid = $_POST['seattypeid'];
			$data->seatnumber = $_POST['seatnumber'];

			if (!isset($data->id)) {
				echo 'Missing event seat id';
				wp_die();
				return;
			}
			if (!isset($data->seattypeid)) {
				echo 'Missing seat type';
				wp_die();
				return;
			}
			if (!isset($data->seatnumber)) {
				echo 'Missing seat number';
				wp_die();
				return;
			}

			
			$context = new afkContext();
			$result = $context->updateEventSeat($data);
			if ($result > 0){
				// success
			}
			else {
				echo 'Failed to update event seat.';
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function add_event_seat() {
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$data = new afkEventSeat();
			
			$data->eventid = $_POST['eventid'];
			$data->positionx = $_POST['positionx'];
			$data->positiony = $_POST['positiony'];
			$data->seattypeid = $_POST['seattypeid'];
			$data->seatnumber = $_POST['seatnumber'];

			if (is_null($data->eventid)) {
				echo 'Missing event id';
				wp_die();
				return;
			}
			if (is_null($data->positionx)) {
				echo 'Missing position x';
				wp_die();
				return;
			}
			if (is_null($data->positiony)) {
				echo 'Missing position y';
				wp_die();
				return;
			}
			if (is_null($data->seattypeid)) {
				echo 'Missing seat type';
				wp_die();
				return;
			}
			if (is_null($data->seatnumber)) {
				echo 'Missing seat number';
				wp_die();
				return;
			}

			
			$context = new afkContext();
			$count = $context->getEventSeatCount($data->eventid, $data->positionx, $data->positiony);
			
			if ($count == 0) {
				$result = $context->addEventSeat($data);
				if ($result > 0){
					//echo 'Event Seat added!';
				}
				else {
					echo 'Failed to add event seat.';
				}
			}
			else {
				echo 'Seat in that position already exists. Cannot add another seat in the same spot.';
			}			
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function delete_event_seat(){
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$data = new afkEventSeat();
			$data->id = $_POST['id'];
			
			$context = new afkContext();
			$seat = $context->getEventSeat($data->id);
			if ($seat->personid > 0) {
				echo 'Delete Failed. This seat is registered to someone';
				wp_die();
				return;
			}
			$result = $context->deleteEventSeat($data);

			if ($result > 0){
				//echo 'Event deleted!';
				
			}
			else {
				echo 'Failed to delete event.  Please try again.';
			}
		}
		else {
			echo 'Missing parameters for delete.';
		}

		wp_die();
	}

	public function add_person_to_event_seat(){
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)){
			$context = new afkContext();
			$seat = $context->getEventSeat($_POST['eventseatid']);
			if (is_null($seat) || !isset($seat)){
				echo 'No seat found.';
				wp_die();
				return;
			}
			if ($seat->personid > 0){
				echo 'Seat is already assigned a participant.';
				wp_die();
				return;
			}

			$person = new afkPerson();
			$person->participantid = $_POST['participantid'];
			$person->name = $_POST['name'];
			$person->email = $_POST['email'];
			$person->specs = $_POST['specs'];

			if (!isset($person->participantid)) {
				echo 'Missing participant id';
				wp_die();
				return;
			}

			if (!isset($person->name)) {
				echo 'Missing name';
				wp_die();
				return;
			}

			if (!isset($person->email)) {
				echo 'Missing email';
				wp_die();
				return;
			}

			
			// make sure participant is not already at this event
			$event = $context->getEvent($seat->eventid);
			foreach ($event->EventSeats as $s) {
				if ($s->Person->participantid == $person->participantid) {
					echo 'This person has already registered for a seat and cannot register another seat.  Please enter a different participant id.';
					wp_die();
					return;
				}
			}

			// find existing person in database
			$oldperson = $context->getPersonByParticipantId($person->participantid);
			if (!is_null($oldperson) && $oldperson->id > 0) {
				// update name email specs
				$oldperson->name = $person->name;
				$oldperson->email = $person->email;
				$oldperson->specs = $person->specs;
				$oldperson = $context->updatePerson($oldperson);

				$result = $context->addPersonToSeat($oldperson, $seat);
				if ($result == false){
					echo 'Failed to assign person to seat';
				}
				else {
					echo $oldperson->name . ' is now registered for a ' . $seat->SeatType->name . ' seat';
					$rseat = $context->getEventSeat($seat->id);
					afkEmailer::sendConfirmationEmail($event, $rseat);
				}
				wp_die();
			}
			
			// no existing person so add one
			$newperson = $context->addPerson($person);
			if ($newperson == false){
				echo 'Failed to create person record.';
			}
			else {
				$result = $context->addPersonToSeat($newperson, $seat);
				if ($result == false){
					echo 'Failed to assign person to seat';
				}
				else {
					echo $newperson->name . ' is now registered for a ' . $seat->SeatType->name . ' seat';
					$rseat = $context->getEventSeat($seat->id);
					afkEmailer::sendConfirmationEmail($event, $rseat);
				}
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function remove_person_from_event_seat(){
		if (wp_validate_auth_cookie() === false) {
			echo 'Unauthorized';
			wp_die();
			return;
		}

		if (!empty($_POST)) {
			$context = new afkContext();
			$seat = $context->getEventSeat($_POST['eventseatid']);

			if (is_null($seat) || !isset($seat)){
				echo 'No seat found.';
				wp_die();
				return;
			}
			if (!isset($seat->personid)){
				echo 'Seat does not have an assigned participant.';
				wp_die();
				return;
			}

			$rows = $context->removePersonFromSeat($seat);
			if ($rows == false || $rows == 0) {
				echo 'Failed to remove the person from the seat.  Please try again.';
			}
			else {
				// send email
				$event = $context->getEvent($seat->eventid);
				afkEmailer::sendRemovalEmail($event, $seat);
			}
		}
		else {
			echo 'Missing or invalid parameters.';
		}
		
		wp_die();
	}

	public function auto_update() {
		$path = 'https://bitbucket.org/wrathburn/alaskafilmmakers/raw/master/dist/version.json';
		$package = 'https://bitbucket.org/wrathburn/alaskafilmmakers/raw/master/dist/afk-xl-reg.zip';
		$slug = 'afk-xl-reg';
		new AFK_XL_REG_UPDATE($this->version, $path, $package, $slug);
	}

	public function enqueue_styles() {
		wp_enqueue_style( $this->afk_xl_reg, plugin_dir_url( __FILE__ ) . 'css/afk-xl-reg-admin.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( $this->afk_xl_reg, plugin_dir_url( __FILE__ ) . 'js/afk-xl-reg-admin.js', array( 'jquery' ), $this->version, false );
	}

}
