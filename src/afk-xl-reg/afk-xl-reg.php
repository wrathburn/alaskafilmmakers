<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wbr.consulting
 * @since             1.0.0
 * @package           AFK_XL_REG
 *
 * @wordpress-plugin
 * Plugin Name:       Extra Life LAN Events
 * Plugin URI:        http://wbr.consulting
 * Description:       A custom plugin for doing Extra Life Lan Party Registration
 * Version:           2.1.0
 * Author:            Wesley Rathburn
 * Author URI:        http://wbr.consulting
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       afk-xl-reg
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $afk_plugin_version;
global $afk_db_version;
$afk_plugin_version = '2.1.0';
$afk_db_version = '2.1.0';
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-afk-xl-reg-activator.php
 */
function activate_afk_xl_reg() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-afk-xl-reg-activator.php';
	AFK_XL_REG_Activator::activate();
}
/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-afk-xl-reg-deactivator.php
 */
function deactivate_afk_xl_reg() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-afk-xl-reg-deactivator.php';
	AFK_XL_REG_Deactivator::deactivate();
}
register_activation_hook( __FILE__, 'activate_afk_xl_reg' );
register_deactivation_hook( __FILE__, 'deactivate_afk_xl_reg' );



/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-afk-xl-reg.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_afk_xl_reg() {
	$plugin = new AFK_XL_REG();
	$plugin->run();
}
run_afk_xl_reg();