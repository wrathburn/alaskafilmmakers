<?php 
    // vars $confirmevent, $confirmseat
    $Person = $confirmseat[0]->Person;
?>
<div class="xl-reg-confirmation">
    <h4>Your Registation is Complete!</h4>
    <p><i>A confirmation email has been sent to the email address below.</i></p>
    <div class="row clearfix">
        <div class="col-md-5">
            <div class="form-group">
                <label class="control-label">Name</label>
                <div class="form-control">
                    <?php echo $Person->name; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <div class="form-control">
                    <?php echo $Person->email; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Extra Life ID</label>
                <div class="form-control">
                    <?php echo $Person->participantid; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Equipment Specs</label>
                <div class="form-control">
                    <?php echo $Person->specs; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Seat(s) </label>
                <?php foreach($confirmseat as &$s){ ?>
                    <div class="form-control"><?php echo $s->seatnumber ?> - <?php echo $s->SeatType->name ?></div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label class="control-label">Terms, Waivers, and Conditions</label>
                <p class="small">
                    <?php echo $confirmevent->terms; ?>
                </p>
            </div>
        </div>
        <div class="col-md-7">
            <div style="padding:15px;border:1px solid #ddd;">
                <div id="newgrid"></div>
            </div>
        </div>
    </div>
    
    
    
    <script type="text/javascript">
        var eventjsonconfirm = <?php echo json_encode($confirmevent); ?>;
        xl.buildGrid('#newgrid', eventjsonconfirm);
        scrollTo(0, jQuery('.xl-reg-confirmation', '#xl-wrapper').position().top);
    </script>
</div>