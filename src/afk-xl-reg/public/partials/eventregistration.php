<?php 

$context = new afkContext();
$seatTypes = 
$event = $context->getEventPublic($args['id']);
if ($event->id > 0 && $event->active) {
    $formid = 'xlform' . $event->id;
?>
<div id="event-form-wrapper">
    <fieldset>
        <h2><?php echo $event->name; ?> Registration</h3>
        <p><b>When:</b> &nbsp;<?php echo date_format(date_create($event->date), "l jS F Y \@ g:ia"); ?></p>
        <p><b>Where:</b> &nbsp;<?php echo $event->text; ?></p>
        <hr />
        <div class="alert alert-danger" style="display:none">
            
        </div>
        <div class="alert alert-success" style="display:none">
            
        </div>
        <div class="form-wrapper" class="clearfix">
            <div class="pull-left" style="width:400px;">
                <form id="<?php echo $formid; ?>">
                    <?php wp_nonce_field( 'submit_xlevent_registration' ); ?>
                    <input type="hidden" name="participantid" id="participantid" value="" required />
                    <input type="hidden" name="eventid" value="<?php echo $event->id; ?>" />
                    <input type="hidden" name="id" required />
                    
                    
                    <div class="step1">
                        <div class="form-group">
                            <label for="xlurl" class="control-label">Paste Your Extra Life Public URL</label>
                            <input type="text" id="xlurl" name="xlurl" required class="form-control" />
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" onclick="getExtraLifeInfo();">Next</button>
                        </div>
                    </div>
                    <div class="loading-info form-group" style="display:none;">Loading your Extra Life Information...</div>
                    
                    <div class="step2" style="display:none;">
                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" name="name" id="name" value="" readonly required class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="raised" class="control-label">Total Raised</label>
                            <input type="text" name="raised" id="raised" value="" readonly class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="seat" class="control-label">Seat #</label>
                            <input type="text" name="seat" value="" placeholder="Select an open seat on the grid." required onfocus="this.blur();" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" name="email" value="" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="specs" class="control-label">Equipment Specs (CPU, Power Supply, Graphics, Monitor, Console Type, etc.)</label>
                            <textarea name="specs" rows="5" required class="form-control"></textarea>
                            <small><i>This information will help event staff to ensure we have adequate power supply for everyone.</i></small>
                        </div>
                        <div class="form-group">
                            <label for="terms">Terms, Waivers, and Conditions</label>
                            <p><?php echo $event->terms; ?></p>
                            <div>
                                <label>
                                    <input type="checkbox" name="termsagree" id="termsagree" />
                                    I Agree to Terms, Waivers, and Conditions
                                </label>
                            </div>
                        </div>
                        <br />
                        <button id="xlsubmit" type="submit" class="btn btn-primary" disabled>Register</button>
                    </div>
                    
                </form>
            </div>
            <div class="pull-left">
                <div class="form-group">
                    <label for="xl-grid" class="control-label">Select An Open Seat</label>
                    <div style="padding:15px;border:1px solid #ddd;">
                        <div id="xl-grid"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="saving-info"></div>
    </fieldset>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        var eventjson = <?php echo json_encode($event); ?>;
        var interval1;
        function getExtraLifeInfo() {

            jQuery('.alert', '#event-form-wrapper').html('').hide();

            var url = jQuery('#xlurl').val();

            if (url.length == 0 || url.toLowerCase().indexOf('participantid') == -1) {
                jQuery('.alert-danger', '#event-form-wrapper').html('Invalid Extra Life Url.  Please try copy and paste your public Extra Life profile url.').show();
                return;
            }

            var id = url.toLowerCase().split('participantid=')[1];
            if (id.indexOf('&') >= 0) 
                id = id.substr(0, id.indexOf('&'));

            var data = {};
            data.ts = Date.now();
            data.participantid = id;
            data.action = 'xl_get_extra_life_json';
            data.eventid = <?php echo $event->id; ?>;

            jQuery('.step1', '#event-form-wrapper').hide();
            jQuery('.loading-info', '#event-form-wrapper').fadeIn(100);

            jQuery.get(ajaxurl, data, function(response) {
                jQuery('.loading-info', '#event-form-wrapper').hide();
                var json = JSON.parse(response);

                if (json.displayName && json.displayName.length > 0){
                    jQuery('#participantid').val(json.participantID);
                    jQuery('#name').val(json.displayName);
                    jQuery('#raised').val('$ ' + json.sumDonations);
                    jQuery('.step1', '#event-form-wrapper').slideUp(150);
                    jQuery('.step2', '#event-form-wrapper').slideDown(150);
                }
                else {
                    jQuery('.alert-danger', '#event-form-wrapper').html('Sorry, an error has occurred. Please try pasting your Extra Life URL again.').show();
                    if (json.error.length > 0) {
                        jQuery('.alert-danger', '#event-form-wrapper').html(json.error).show();
                    }
                    jQuery('.step1', '#event-form-wrapper').fadeIn(100);
                    jQuery('#xlurl').val('');
                }

            }).fail(function(){
                jQuery('.alert-danger', '#event-form-wrapper').html('Sorry, an error has occurred. Please refresh this page and try again.').show();
                jQuery('.loading-info', '#event-form-wrapper').hide();
                jQuery('.step1', '#event-form-wrapper').fadeIn(100);
                jQuery('#xlurl').val('');
            });
        }

        jQuery(function(){

            buildGrid('#xl-grid', eventjson);

            jQuery('.grid-cell.available', '#xl-grid').on('click', function(){
                var $this = jQuery(this);
                jQuery('.grid-cell', '#xl-grid').removeClass('active');
                $this.addClass('active');

                var data = $this.data('json');
                var form = jQuery('form', '#event-form-wrapper')[0];
                form.elements.id.value = data.id;
                form.elements.seat.value = data.seatnumber + ' - ' + data.SeatType.name;
                jQuery(form).change();
            });

            jQuery('form', '#event-form-wrapper').on('submit', function(e){
                e.preventDefault();
                jQuery('.alert', '#event-form-wrapper').html('').hide();
                if (this.checkValidity() && this.elements.id.value > 0 && this.elements.eventid.value > 0 && this.elements.participantid.value > 0 && this.elements.termsagree.checked){
                    var data = {};
                    data.action = 'xl_submit_registration';
                    data._wpnonce = this.elements._wpnonce.value;
                    data._wp_http_referer = this.elements._wp_http_referer.value;
                    data.participantid = this.elements.participantid.value;
                    data.eventid = this.elements.eventid.value;
                    data.id = this.elements.id.value;
                    data.name = this.elements.name.value;
                    data.email = this.elements.email.value;
                    data.specs = this.elements.specs.value;
                    jQuery('.form-wrapper', '#event-form-wrapper').hide();
                    
                    jQuery('.saving-info', '#event-form-wrapper').html('Saving Please Wait...').show();
                    interval1 = setInterval(function(){
                        var e = jQuery('.saving-info', '#event-form-wrapper');
                        if (e.length > 0) e.append('.');
                        else clearInterval(interval1);
                    }, 333);

                    jQuery.post(ajaxurl, data, function(response){
                        jQuery('.form-wrapper', '#event-form-wrapper').remove();
                        jQuery('.saving-info', '#event-form-wrapper').remove();
                        jQuery('fieldset', '#event-form-wrapper').append(response);
                    }).fail(function(){
                        
                        alert('Sorry, but a server error has occurred.  This page will refresh so you can try to register again.');
                        window.location.reload(true);
                    });
                }
                else {
                    jQuery('.alert-danger', '#event-form-wrapper').html('Please complete all required fields.').show().focus();
                }
            });

            jQuery('form', '#event-form-wrapper').on('change', function() {
                var valid = this.checkValidity() && this.elements.id.value > 0 && this.elements.eventid.value > 0 && this.elements.participantid.value > 0 && this.elements.termsagree.checked;
                jQuery('#xlsubmit', this).prop('disabled', !valid);
            });

        });
    </script>
</div>

<?php } else { 
?>

<div id="event-form-wrapper">
    <fieldset>
        <h2><?php echo $event->name; ?> Registration</h3>
        <p><b>When:</b> &nbsp;<?php echo date_format(date_create($event->date), "l jS F Y \@ g:ia"); ?></p>
        <p><b>Where:</b> &nbsp;<?php echo $event->text; ?></p>
        <hr />
        <h3>Registration is closed for this event.</h3>
        <div class="form-group">
            <div style="padding:15px;border:1px solid #ddd;">
                <div id="xl-grid"></div>
            </div>
        </div>
    </fieldset>
    <script type="text/javascript">
        
        var eventjson = <?php echo json_encode($event); ?>;

        jQuery(function(){
            buildGrid('#xl-grid', eventjson);
        });
    </script>
</div>
<?php

}
?>



