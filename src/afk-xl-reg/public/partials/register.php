<?php 
    $context = new afkContext();
    $event = $context->getEventPublic($args['id']);
    if ($event->id > 0 && $event->active) { 
?>
    <div id="xl-wrapper">
        
        <fieldset>
            <h2 class="xl-font"><?php echo $event->name; ?> Registration</h3>
            <p class="xl-font"><b>When:</b> &nbsp;<?php echo date_format(date_create($event->date), "l jS F Y \@ g:ia"); ?></p>
            <p class="xl-font"><b>Where:</b> &nbsp;<?php echo $event->text; ?></p> 
            <hr />
            <div class="start-over form-group" style="display:none;">
                <a href="javascript:void(0);" onclick="xl.startOver();">Start Over</a>
            </div>
            <div class="alert alert-danger" style="display:none"></div>
            <div class="alert alert-success" style="display:none"></div>
            <div class="form-wrapper row">
                <div class="col-xs-12 col-sm-12 col-md-5">
                    <div id="xl-form">
                        <div id="xl-step1" class="form-group">
                            <label class="control-label text-uppercase">Are you registering an Individual or a Team?</label>
                            <div>
                                <button type="button" class="btn btn-lg btn-default" onclick="xl.showIndividual();">INDIVIDUAL</button>
                                <button type="button" class="btn btn-lg btn-default" onclick="xl.showTeam()">TEAM</button>
                            </div>
                        </div>
                        <div id="xl-form-individual" style="display:none">
                            <h3>Individual Registration</h4>
                            
                            <div class="form-group search">
                                <label class="control-label">Extra-Life Account Search</label>
                                <div><i><small>Enter your Extra Life display name, Extra Live ID #, or paste your public profile URL.</small></i></div>
                                <div class="clearfix">
                                    <input type="text" class="form-control search-box text-uppercase pull-left" style="text-transform:uppercase;float:left;width:70%;" onkeydown="if(event.keyCode === 13) xl.doIndividualSearch();" />
                                    <button type="button" class="btn btn-default pull-left" onclick="xl.doIndividualSearch()" style="float:left;width:30%;">Search</button>
                                </div>
                                
                            </div>

                            <div class="form-group search search-results"></div>

                            <div class="form-group fields" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <div class="form-control-static">
                                        <img class="thumbnail avatarImageURL" />
                                        <span class="displayName xl-font" style="font-size:24px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Raised</label>
                                    <div class="form-control-static">
                                        <span class="sumDonations xl-font" style="font-size:24px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Extra Live ID</label>
                                    <div class="form-control-static">
                                        <span class="participantID xl-font"></span>
                                    </div>
                                </div>
                                
                                <div class="form-group xl-input">
                                    <label class="control-label" for="email">*Email</label>
                                    <input type="email" name="email" id="xl-email" class="form-control" required />
                                </div>
                                <div class="form-group xl-input">
                                    <label class="control-label" for="specs">*Equipment Specs </label>
                                    <div><small><i>This information will help event staff to ensure we have adequate power supply for everyone. (CPU, Power Supply, Graphics, Monitor, Console Type, etc.) </i></small></div>
                                    <textarea name="specs" id="xl-specs" class="form-control" rows="3" required placeholder="CPU, Power Supply, Graphics, Monitor, Console Type, etc."></textarea>
                                </div>
                                <div class="bonus form-group text-bold text-success">Awesome! You raised enough to register for extra seat(s).</div>
                                <div class="no-bonus form-group text-bold text-warning">Hey! If you raise just a bit more money, you can register for extra seats.</div>
                                <div class="form-group xl-input">
                                    <label class="control-label">Selected Seat(s)</label>
                                    <div class="form-control">
                                        <b class="seats text-uppercase" style="font-size:125%"></b>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div id="xl-form-team" style="display:none"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7">
                    <div id="xl-grid-wrapper" class="form-group" style="display:none;">
                        <label for="xl-grid" class="control-label">Select An Open Seat</label>
                        <div style="padding:15px;border:1px solid #ddd;overflow-x:auto;">
                            <div id="xl-grid"></div>
                        </div>
                    </div>
                </div>
                <div id="xl-submit-wrapper" class="col-md-12" style="display:none;">
                    <div class="form-group">
                        <label class="control-label">Terms, Waivers, and Conditions</label>
                        <p class="small"><?php echo $event->terms; ?></p>
                        <div>
                            <label>
                                <input type="checkbox" id="termsagree" />
                                I Agree to Terms, Waivers, and Conditions
                            </label>
                        </div>
                    </div>
                    <button type="button" id="xl-submit" class="btn btn-primary xl-submit" >SUBMIT REGISTRATION</button>
                </div>
            </div>
        </fieldset>
        <div class="saving-info" style="display:none;text-align:center">
            <div class="mk-spinner-wrap" style="text-align:center;"><i class="mk-spinner-doublecircle"></i></div>
            <h5>Saving Please Wait</h5>
        </div>
        <script type="text/javascript">
            jQuery(function(){
                xl.init(
                    '<?php echo admin_url('admin-ajax.php'); ?>', 
                    '<?php echo wp_create_nonce( 'submit_xlevent_registration' ) ?>', 
                    <?php echo json_encode($event); ?>
                );
            });
        </script>
    </div>

<?php } else {  ?>
    <div id="xl-wrapper">
        <fieldset>
            <h2><?php echo $event->name; ?> Registration</h3>
            <p><b>When:</b> &nbsp;<?php echo date_format(date_create($event->date), "l jS F Y \@ g:ia"); ?></p>
            <p><b>Where:</b> &nbsp;<?php echo $event->text; ?></p>
            <hr />
            <h3>Registration is closed for this event.</h3>
        </fieldset>
    </div>
<?php } ?>