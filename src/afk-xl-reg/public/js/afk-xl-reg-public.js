/*jshint multistr: true */
(function($, xl) {

    // vars
    var interval = null;
    var xlobject = {};

    // extra life api calls
    xl.searchParticipant = function(text, callback) {
        $.get('https://www.extra-life.org/api/participants?limit=10&where=' + encodeURIComponent("displayName = '" + text + "'"), function(data) {
            callback(data);
        }).fail(function() {
            xl.showError('Sorry, We were unable to connect to Extra Life to find your information.  Please refresh this page and try again.');
        });
    };

    xl.getParticipant = function(id, callback) {
        $.get('https://www.extra-life.org/api/participants/' + id, function(data) {
            callback(data);
        }).fail(function() {
            xl.showError('Sorry, We were unable to connect to Extra Life to find your information.  Please refresh this page and try again.');
        });
    };

    xl.getAvatarImageURL = function(id, callback) {
        $.get('https://www.extra-life.org/api/participants/' + id + "?select=avatarImageURL", function(data) {
            if (data !== undefined && data !== "")
                callback(data.avatarImageURL);
        }).fail(function() {
            return;
        });
    };

    xl.searchTeam = function(text, callback) {
        $.get('https://www.extra-life.org/api/teams?limit=10&where=' + encodeURIComponent("name = '" + text + "'"), function(data) {
            callback(data);
        }).fail(function() {
            xl.showError('Sorry, We were unable to connect to Extra Life to find your information.  Please refresh this page and try again.');
        });
    };

    xl.getTeam = function(id, callback) {
        $.get('https://www.extra-life.org/api/teams/' + id, function(data) {
            callback(data);
        }).fail(function() {
            xl.showError('Sorry, We were unable to connect to Extra Life to find your information.  Please refresh this page and try again.');
        });
    };

    xl.getExtraLifeInfo = function() {

        $('.alert', '#xl-wrapper').html('').hide();

        var url = $('#xlurl').val();

        if (url.length === 0 || url.toLowerCase().indexOf('participantid') < 0) {
            $('.alert-danger', '#xl-wrapper').html('Invalid Extra Life Url.  Please try copy and paste your public Extra Life profile url.').show();
            return;
        }

        var id = url.toLowerCase().split('participantid=')[1];
        if (id.indexOf('&') >= 0)
            id = id.substr(0, id.indexOf('&'));

        var data = {};
        data.ts = Date.now();
        data.participantid = id;
        data.action = 'xl_get_extra_life_json';
        data.eventid = xlobject.event.id;

        $('.step1', '#xl-wrapper').hide();
        $('.loading-info', '#xl-wrapper').fadeIn(100);

        $.get(xlobject.ajaxurl, data, function(response) {
            $('.loading-info', '#xl-wrapper').hide();
            var json = JSON.parse(response);

            if (json.displayName && json.displayName.length > 0) {
                $('#participantid').val(json.participantID);
                $('#name').val(json.displayName);
                $('#raised').val('$ ' + json.sumDonations);
                $('.step1', '#xl-wrapper').slideUp(150);
                $('.step2', '#xl-wrapper').slideDown(150);
            } else {
                $('.alert-danger', '#xl-wrapper').html('Sorry, an error has occurred. Please try pasting your Extra Life URL again.').show();
                if (json.error.length > 0) {
                    $('.alert-danger', '#xl-wrapper').html(json.error).show();
                }
                $('.step1', '#xl-wrapper').fadeIn(100);
                $('#xlurl').val('');
            }

        }).fail(function() {
            $('.alert-danger', '#xl-wrapper').html('Sorry, an error has occurred. Please refresh this page and try again.').show();
            $('.loading-info', '#xl-wrapper').hide();
            $('.step1', '#xl-wrapper').fadeIn(100);
            $('#xlurl').val('');
        });
    };

    xl.showError = function(message) {
        $('.alert', '#xl-wrapper').html('').hide();
        var y = $('.alert-danger', '#xl-wrapper').first().html(message).show().position().top;
        window.scrollTo(0,y);
    };

    xl.showSuccess = function(message) {
        $('.alert', '#xl-wrapper').html('').hide();
        $('.alert-success', '#xl-wrapper').first().html(message).show().focus();
    };

    xl.startOver = function() {
        $('#xl-form-individual, #xl-form-team, #xl-grid-wrapper, .fields, .alert, .start-over', '#xl-wrapper').hide();
        $('.avatarImageURL', '#xl-wrapper').attr('src', null);
        $('.search-box, .displayName, .participantID, .sumDonations', '.alert', '#xl-wrapper').html('');
        $('#xl-step1', '#xl-wrapper').slideDown();
        $('.grid-cell.available', '#xl-grid').off('click').removeClass('active');
    };

    xl.spinner = '<div class="mk-spinner-wrap" style="text-align:center;"><i class="mk-spinner-doublecircle"></i></div>';

    xl.hideAlerts = function() {
        $('.alert', '#xl-wrapper').html('').hide();

    };

    // individual registration
    xl.showIndividual = function() {
        $('#xl-step1', '#xl-wrapper').hide();
        $('div.search', '#xl-wrapper').show();
        $('input.search-box', '#xl-wrapper').val('').focus();
        $('div.search-results', '#xl-wrapper').html('');
        $('#xl-form-individual, .start-over', '#xl-wrapper').slideDown();
        $('input.search-box', '#xl-wrapper').focus();
    };



    xl.doIndividualSearch = function() {
        var text = $('input.search-box', '#xl-form-individual').val();
        var id = parseInt(text);
        $('.search-results', '#xl-form-individual').html(xl.spinner);
        xl.hideAlerts();

        if (text.toLowerCase().indexOf('participantid=') >= 0) {
            text = text.toLowerCase();
            text = text.substring(text.indexOf('participantid=')).substring(14);
            if (text.indexOf('&') >= 0) {
                text = text.substring(0, text.indexOf('&'));
            }
            id = parseInt(text);
        }

        if (!isNaN(id) && id > 0) {
            xl.getParticipant(id, xl.individualSearchCallback);
            return;
        }

        xl.searchParticipant(text, xl.individualSearchCallback);

    };

    xl.individualSearchCallback = function(data) {
        var $div = $('div.search-results', '#xl-form-individual').first();
        $div.html('');

        if (data === undefined || data === null || data.length === 0) {
            xl.showError("Sorry we could not find your extra life account.  Please type your name exactly as it is displayed in your extra life account.");
            return;
        }

        if (data.length === 1) {
            data = data[0];
        }

        if (data.participantID > 0) {
            // single result
            xl.initIndividualForm(data);
        } else if (data.length > 1) {
            // multi result
            $div.append('<div class="form-group"><i>Multiple results found, please choose your account:</i></div>');
            var $table = $('<table class="table table-condensed table-hover small" style="font-size:small"></table>');
            data.forEach(function(item) {
                var $item = $('<tr>\
                <td><button type="button" class="btn btn-sm btn-default">SELECT</button></td>\
                <td><img style="width:30px;" class="thumbnail" /></td>\
                <td class="i-displayName"></td>\
                <td class="i-eventName"></td>\
                <td class="i-sumDonations"></td>\
                </tr>');
                $item.find('td.i-displayName').html(item.displayName);
                $item.find('td.i-eventName').html(item.eventName);
                $item.find('td.i-sumDonations').html('$' + item.sumDonations + ' Raised');
                $item.find('img').attr('src', item.avatarImageURL);
                $item.find('button').val(item.participantID).data('item', item).on('click', function() {
                    xl.individualSearchCallback($(this).data('item'));
                });
                $item.data('item', item);
                $table.append($item);
            });

            $div.append($table);
        }
    };

    xl.initIndividualForm = function(data) {
        var canRegister = true;
        var $fields = $('.fields', '#xl-form-individual');

        if (data.sumDonations < xlobject.event.minraisedamount) {
            xl.showError('Sorry, you have not raised enough to register.  The minimum is $' + xlobject.event.minraisedamount);
            canRegister = false;
            $fields.find('.xl-input').hide();
        }

        $('.search', '#xl-form-individual').hide();

        $fields.find('.bonus, .no-bonus').hide();


        // api object fields 
        // avatarImageURL, displayName, eventName, fundraisingGoal, numDonations, sumDonations, participantID
        $fields.find('.displayName').html(data.displayName);
        $fields.find('.avatarImageURL').attr('src', data.avatarImageURL);
        $fields.find('.participantID').html(data.participantID);
        $fields.find('.sumDonations').html('$ ' + data.sumDonations);
        $fields.find('#xl-email,#xl-specs').val('');
        $fields.find('.seats').html('');

        if (canRegister) {
            var maxSeats = 1;
            if (xlobject.event.bonusamountseats > 0 &&
                xlobject.event.bonusamount > 0) {
                // bonus seats are available
                if (data.sumDonations >= xlobject.event.bonusamount) {
                    $fields.find('.bonus').html('Awesome! You have raised enough to register for your seat plus ' + xlobject.event.bonusamountseats + ' extra seat(s).').show();
                    maxSeats += parseInt(xlobject.event.bonusamountseats);
                } else {
                    var diff = xlobject.event.bonusamount - data.sumDonations;
                    $fields.find('.no-bonus').html('Hey! If you raise just a $' + parseInt(diff) + ' more dollars, you can register for ' + xlobject.event.bonusamountseats + ' extra seat(s).').show();
                }
            }

            // init registration object
            xlobject.registration = {};
            xlobject.registration._wpnonce = xlobject._wpnonce;
            xlobject.registration.action = 'xl_submit_registration';
            xlobject.registration.type = "individual";
            xlobject.registration.eventid = xlobject.event.id;
            xlobject.registration.participantid = data.participantID;
            xlobject.registration.name = data.displayName;
            xlobject.registration.specs = null;
            xlobject.registration.email = null;
            xlobject.registration.seats = new Array(0);
            xlobject.registration.maxSeats = maxSeats;
            


            // listener for fields
            $fields.find('#xl-email').on('keyup', function() {
                xlobject.registration.email = this.value;
            });

            $fields.find('#xl-specs').on('keyup', function() {
                xlobject.registration.specs = this.value;
            });

            // clear event handler and re-bind for individual event registration
            $('.grid-cell.available', '#xl-grid').off('click').removeClass('active');
            $('.grid-cell.available', '#xl-grid').on('click', function() {
                var $this = $(this);
                var seatdata = $(this).data('json');

                if ($this.hasClass('active')) {
                    // toggle active off
                    $this.removeClass('active');
                    $fields.find('.seats > .seat[data-id="' + seatdata.id + '"]').remove();
                    xlobject.registration.seats = xlobject.registration.seats.filter(function(item) { return item !== seatdata.id; });
                } else {
                    if (xlobject.registration.seats.length >= xlobject.registration.maxSeats) {
                        // remove first element and resize
                        var removeid = xlobject.registration.seats.shift();
                        $('.grid-cell.available.active[data-id="' + removeid + '"]').removeClass('active');
                        $fields.find('.seats > .seat[data-id="' + removeid + '"]').remove();
                    }

                    // add seat to registration
                    $this.addClass('active');
                    $fields.find('.seats').append(' <span class="seat" data-id="' + seatdata.id + '">' + seatdata.seatnumber + '</span> ');
                    xlobject.registration.seats.push(seatdata.id);
                }
            });

            $('#xl-submit', '#xl-wrapper').off('click').on('click', function() {
                xl.hideAlerts();
                // validate
                if (xlobject.registration.email === null || xlobject.registration.length < 3 || !$('#xl-email')[0].checkValidity()) {
                    xl.showError("Please enter a valid email address.");
                    return false;
                }

                if (xlobject.registration.specs === null || xlobject.registration.length < 3 || !$('#xl-specs')[0].checkValidity()) {
                    xl.showError("Please enter your computer or console specs.");
                    return false;
                }

                if (xlobject.registration.seats.length === 0) {
                    xl.showError("Please select a seat.  You may select up to " + xlobject.registration.maxSeats + " seat(s).");
                    return false;
                }

                if (!$('#termsagree').is(':checked')) {
                    xl.showError('Please agree to Terms, Waivers, and Conditions.')
                    return false;
                }

                // submit
                $('fieldset', '#xl-wrapper').children().hide();
                $('.saving-info', '#xl-wrapper').slideDown();
                
                $.post(xlobject.ajaxurl, xlobject.registration, function(response) {
                    $('.saving-info', '#xl-wrapper').hide();
                    $('fieldset', '#xl-wrapper').html('').show().append(response);
                }).fail(function() {
                    alert('Sorry, an error has occurred while saving your registration.  This page will refresh so you can try again.');
                    window.location.reload(true);
                });
            });
            $('#xl-submit-wrapper').show();

            $('#xl-grid-wrapper').show();
            $fields.find('.xl-input').show();
        }

        $fields.slideDown(function() {
            $fields.find('#xl-email').focus();
        });

    };





    // team registration
    xl.showTeam = function() {
        xl.showError('Team Registration Comming Soon!');
    };

    // grid
    xl.buildGrid = function(grid, json) {
        grid = $(grid);
        if (json === null || json === undefined) {
            json = xlobject.event;
        }
        var w = parseInt(json.gridwidth);
        var h = parseInt(json.gridheight);


        grid.html('').addClass('grid')
            .width((w * 25))
            .height((h * 25))
            .css('background-image', 'url(' + json.backgroundimage + ')')
            .css('min-width', (w * 25) + 'px');

        for (var i = 0; i < h; i++) {
            var row = $('<div class="grid-row clearfix"></div>');
            for (var j = 0; j < w; j++) {
                row.append('<div class="grid-cell" data-positiony="' + i + '" data-positionx="' + j + '"></div>');
            }
            grid.append(row);
        }

        var seattypes = [];
        json.EventSeats.forEach(function(e) {
            var cell = $('.grid-cell[data-positionx="' + e.positionx + '"][data-positiony="' + e.positiony + '"]', grid).first();
            cell.data('id', e.id).attr('data-id', e.id).data('seattypeid', e.seattypeid).data('personid', e.personid).data('json', e);

            cell.attr('title', e.seatnumber).html(e.seatnumber);
            cell.css('background-color', e.SeatType.color);

            if (e.personid > 0) {
                cell.addClass('taken');
                cell.attr('title', e.seatnumber + ' - Registered to ' + e.Person.name + ' Click to view Extra Life Page');
                cell.attr('data-participantid', e.Person.participantid);
                cell.attr('onclick', 'window.open("https://www.extra-life.org/index.cfm?fuseaction=donordrive.participant&participantID=" + ' + e.Person.participantid + ');');
            } else {
                cell.addClass('available');
            }

            if (seattypes.map(function(p) { return p.id; }).indexOf(e.seattypeid) < 0) {
                seattypes.push(e.SeatType);
            }

        });

        // build legend
        var legend = $('<div class="grid-legend"></div>');
        seattypes.forEach(function(e) {
            legend.append('<div class="grid-legend-item"><span style="background-color:' + e.color + '"></span> ' + e.name + '</div>');
        });
        grid.parent().before(legend);

        // load extra-life images
        $('.grid-cell[data-participantid]').each(function() {
            xl.getAvatarImageURL($(this).data('participantid'), function(url) {
                $(this).css("background-image", 'url("' + url + '")').css('background-size', '95%').css('background-repeat', 'no-repeat').css('background-position', 'center center');
            }.bind(this));

        });
    };

    // initialize, call from document ready
    xl.init = function(ajaxurl, nonce, eventjson) {
        xlobject.ajaxurl = ajaxurl;
        xlobject._wpnonce = nonce;
        xlobject.event = eventjson;
        xl.buildGrid('#xl-grid', xlobject.event);
    };

}(jQuery, window.xl = window.xl || {}));