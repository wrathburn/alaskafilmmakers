<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/public
 * @author     Your Name <email@example.com>
 */
class AFK_XL_REG_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $afk_xl_reg    The ID of this plugin.
	 */
	private $afk_xl_reg;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $afk_xl_reg       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $afk_xl_reg, $version ) {

		$this->afk_xl_reg = $afk_xl_reg;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->afk_xl_reg, plugin_dir_url( __FILE__ ) . 'css/afk-xl-reg-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->afk_xl_reg, plugin_dir_url( __FILE__ ) . 'js/afk-xl-reg-public.js', array( 'jquery' ), $this->version, false );

	}

	public function register_shortcodes(){
		// shortcode add
		add_shortcode( 'xlevent', array($this, 'event_registration_form') );
	}

	public function get_partial() {
		if(isset($_GET['partial'])) {
			switch ($_GET['partial']) {
				case 'event':
					include 'partials/event.php';
					break;
				default:
					wp_die('Nothing Found');
					break;
			}
			die();
		}
	}

	public function event_registration_form($attr){
		ob_start();
		$args = shortcode_atts(array('id' => 0), $attr);
		include('partials/register.php');
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	public function get_extra_life_json() {
		if (isset($_GET['participantid']) && isset($_GET['eventid'])) {
			$participantid = $_GET['participantid'];
			$eventid = $_GET['eventid'];
			$context = new afkContext();
			$event = $context->getEventPublic($eventid);
			if (!is_null($event) && $event->id > 0){
				$json =  $this->get_extra_life_json_by_value($participantid);

				$data = json_decode($json);
				if ($json == '{}'){
					wp_send_json('{"error":"Sorry we were unable to retrieve your Extra Life information.  Please make sure you have copy and pasted your full public Extra Life URL."}');
					return;
				}

				if ($data->sumDonations >= $event->minraisedamount) {

					if ($this->is_registered($event, $participantid)){
						wp_send_json('{"error":"Hi ' . $data->displayName . '! You have already registered a seat for this event.  Please invite your friends to join Extra Life and register for this event."}');
						return;
					}

					wp_send_json($json);
					return;
				}
				wp_send_json('{"error":"Sorry, you have not raised enough donations to register.  The minimum is $' . number_format($event->minraisedamount, 2) . '.  You can make a donation to yourself to reach the minimum and register."}');
				return;
			}
			wp_send_json('{"error":"Invalid Event information.  Please refresh this page and try again."}');
			return;
			
		}
		wp_send_json('{"error":"Invalid Extra Life information.  Please try again."}');
	}

	public function get_extra_life_json_by_value($participantid){
		$url = "https://www.extra-life.org/api/participants/" . $participantid;
		$json = file_get_contents($url);
		return $json;
	}

	public function is_registered($event, $participantid){
		foreach ($event->EventSeats as $seat) {
			if ($seat->Person->participantid == $participantid){
				return true;
			}
		}
		return false;
	}

	public function submit_registration(){
		if (wp_verify_nonce($_POST['_wpnonce'], 'submit_xlevent_registration' )) {
			if (!empty($_POST)) {
				$registrationType = $_POST['type'];
				if ($registrationType == 'individual'){
					$this->individual_registration();
				}
				wp_die('');
			}
			else {
				wp_die('Whoops something was invalid on the page.  Please reload this page and try again.');
			}
		}
		else{
			wp_die('Whoops something was invalid on the page.  Please reload this page and try again.');
		}

		
	}


	public function individual_registration() {
		$context = new afkContext();
		$seats = $_POST['seats'];
		$event = $context->getEvent($_POST['eventid']);
		$seatCount = 0;
		foreach ($seats as &$s) {
			$seatCount++;
			$s = $context->getEventSeat($s);
			if (is_null($s) || !isset($s)){
				wp_die('Whoops, the seat you selected is invalid.  Please refresh this page and try again.');
				return;
			}
			if ($s->personid > 0){
				wp_die('Sorry, but this seat is already assigned.  Please refresh this page and try again.');
				return;
			}
		}
		

		$person = new afkPerson();
		$person->participantid = $_POST['participantid'];
		$person->name = $_POST['name'];
		$person->email = $_POST['email'];
		$person->specs = stripslashes($_POST['specs']);

		if (!isset($person->participantid)) {
			wp_die('Missing Extra Life id.  Please refresh this page and try again.');
			return;
		}

		if (!isset($person->name)) {
			wp_die('Name field is required.  Please refresh this page and try again.');
			return;
		}

		if (!isset($person->email)) {
			wp_die('Email field is required.  Please refresh this page and try again.');
			return;
		}

		if (!isset($person->specs)) {
			wp_die('Equipment specs field is required.  Please refresh this page and try again.');
			return;
		}
		
		
		

		// pull extra life information again to double check
		$xljson = $this->get_extra_life_json_by_value($person->participantid);
		$data = json_decode($xljson);
		$person->name = $data->displayName;
		

		if ($xljson == '{}' || empty($data->participantID)) {
			wp_die('Sorry we were unable to retrieve your Extra Life information.  Please make sure you have copy and pasted your full public Extra Life URL. Please refresh this page and try again.');
			return;
		}

		if ($data->sumDonations < $event->minraisedamount) {
			wp_die('Sorry, you have not raised enough donations to register.  The minimum is $' . number_format($event->minraisedamount, 2) . '.  You can make a donation to yourself to reach the minimum and register');
			return;
		}

		// make sure participant is not already at this event
		if ($this->is_registered($event, $person->participantid)) {
			wp_die('Hi ' . $data->displayName . '! You have already registered a seat for this event.  Please invite your friends to join Extra Life and register for this event.');
			return;
		}

		// check bonus seat amounts count and validate
		if ($seatCount > 1) {
			$maxSeats = $event->bonusamountseats + 1;
			if ($seatCount > $maxSeats){
				wp_die('One to many seats were selected please start over and try again.');
				return;
			}

			if ($data->sumDonations < $event->bonusamount){
				wp_die('Sorry, you have not raised enough to register for extra seats.');
				return;
			}

		}


		// find existing person in database
		$person->id = 0;
		$oldperson = $context->getPersonByParticipantId($person->participantid);
		if (!is_null($oldperson) && $oldperson->id > 0) {
			// update name email specs
			$oldperson->name = $person->name;
			$oldperson->email = $person->email;
			$oldperson->specs = $person->specs;
			$oldperson = $context->updatePerson($oldperson);
			if ($oldperson == false){
				$person->id = 0;
			}
			else{
				$person = $oldperson;
			}
			
		}

		// no existing person so add one
		if ($person->id == 0) {
			$person = $context->addPerson($person);
			if ($person == false){
				wp_die('Sorry, an error occurred while trying to save your registration.  Please refresh this page and try again. 2');
				return;
			}
		}
		
		$success = true;
		foreach($seats as &$s)
		{
			// map person to seat(s)
			$result = $context->addPersonToSeat($person, $s);
			if ($result == false){
				$success = false;
				$context->removePersonFromAllSeats($person);
				break;
			}
		}

		if ($success)
		{
			// re-query seats from database
			foreach($seats as &$s){
				$s = $context->getEventSeat($s->id);
			}
			
			// success registration
			$confirmseat = $seats;
			$confirmevent = $context->getEventPublic($confirmseat[0]->eventid);
			afkEmailer::sendConfirmationEmail($confirmevent, $confirmseat);
			include 'partials/confirmation.php';
		}
		else{
			$context->removePersonFromAllSeats($person);
			wp_die('Sorry an error occurred saving your seats.  Please refresh this page and try again.');
		}
	}
}
