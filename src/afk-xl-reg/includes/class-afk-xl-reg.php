<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 * @author     Your Name <email@example.com>
 */
class AFK_XL_REG {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      AFK_XL_REG_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $afk_xl_reg    The string used to uniquely identify this plugin.
	 */
	protected $afk_xl_reg;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		global $afk_plugin_version;
		$this->afk_xl_reg = 'afk-xl-reg';
		$this->version = $afk_plugin_version;

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - AFK_XL_REG_Loader. Orchestrates the hooks of the plugin.
	 * - AFK_XL_REG_i18n. Defines internationalization functionality.
	 * - AFK_XL_REG_Admin. Defines all hooks for the admin area.
	 * - AFK_XL_REG_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-afk-xl-reg-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-afk-xl-reg-update.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-afk-xl-reg-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-afk-xl-reg-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-afk-xl-reg-public.php';


		$this->loader = new AFK_XL_REG_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the AFK_XL_REG_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new AFK_XL_REG_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new AFK_XL_REG_Admin( $this->get_afk_xl_reg(), $this->get_version() );

		
		$this->loader->add_action( 'init', $plugin_admin, 'auto_update' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'create_menu' );
		// partial loader
		$this->loader->add_action( 'wp_ajax_afk_get_partial', $plugin_admin, 'get_partial');
		// crud event
		$this->loader->add_action( 'wp_ajax_add_event', $plugin_admin, 'add_event');
		$this->loader->add_action( 'wp_ajax_update_event', $plugin_admin, 'update_event');
		$this->loader->add_action( 'wp_ajax_delete_event', $plugin_admin, 'delete_event');
		// crud seat type
		$this->loader->add_action( 'wp_ajax_add_seat_type', $plugin_admin, 'add_seat_type');
		$this->loader->add_action( 'wp_ajax_update_seat_type', $plugin_admin, 'update_seat_type');
		$this->loader->add_action( 'wp_ajax_delete_seat_type', $plugin_admin, 'delete_seat_type');
		// crud event seat
		$this->loader->add_action( 'wp_ajax_add_event_seat', $plugin_admin, 'add_event_seat');
		$this->loader->add_action( 'wp_ajax_update_event_seat', $plugin_admin, 'update_event_seat');
		$this->loader->add_action( 'wp_ajax_delete_event_seat', $plugin_admin, 'delete_event_seat');
		// crud person to seat
		$this->loader->add_action( 'wp_ajax_add_person_to_event_seat', $plugin_admin, 'add_person_to_event_seat');
		$this->loader->add_action( 'wp_ajax_remove_person_from_event_seat', $plugin_admin, 'remove_person_from_event_seat');
		
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new AFK_XL_REG_Public( $this->get_afk_xl_reg(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'init', $plugin_public, 'register_shortcodes');

		$this->loader->add_action( 'wp_ajax_afk_get_partial', $plugin_public, 'get_partial');
		$this->loader->add_action( 'wp_ajax_xl_submit_registration', $plugin_public, 'submit_registration');
		$this->loader->add_action( 'wp_ajax_xl_get_extra_life_json', $plugin_public, 'get_extra_life_json');

		$this->loader->add_action( 'wp_ajax_nopriv_afk_get_partial', $plugin_public, 'get_partial');
		$this->loader->add_action( 'wp_ajax_nopriv_xl_submit_registration', $plugin_public, 'submit_registration');
		$this->loader->add_action( 'wp_ajax_nopriv_xl_get_extra_life_json', $plugin_public, 'get_extra_life_json');

		
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_afk_xl_reg() {
		return $this->afk_xl_reg;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    AFK_XL_REG_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}


// models
class afkEvent{
	public $id;
	public $date;
	public $name;
	public $text;
	public $url;
	public $gridwidth;
	public $gridheight;
	public $backgroundimage;
	public $adminemail;
	public $active;
	public $minraisedamount;
	public $bonusamount;
	public $bonusamountseats;
	public $terms;

	public $EventSeats;
}

class afkSeatType{
	public $id;
	public $name;
	public $color;
}

class afkPerson {
	public $id;
	public $name;
	public $email;
	public $participantid;
	public $specs;
}

class afkEventSeat {
	public $id;
	public $eventid;
	public $positionx;
	public $positiony;
	public $seattypeid;
	public $personid;
	public $seatnumber;

	public $Event;
	public $SeatType;
	public $Person;
}

class afkEmailer {
	public static function sendConfirmationEmail($event, $eventSeats){
		$edate = date_format(date_create($event->date), 'l F jS\, Y');

		$Person = null;
		$seatLines = '';

		if (is_array($eventSeats)){
			// multi seat confirmation
			$Person = $eventSeats[0]->Person;
			foreach ($eventSeats as $s){
				$seatLines = $seatLines.'Seat: '.$s->seatnumber.' - '.$s->SeatType->name.' 
';
			}
		} else{
			// single seat confirmation
			$Person = $eventSeats->Person;
			$seatLines = 'Seat: '.$eventSeats->seatnumber.' - '.$eventSeats->SeatType->name.' 
';
		}


		$message = 'Thank you for registering for ' . $event->name . ' on ' . $edate . '!  

Event Details: ' . $event->text . '


Name: '.$Person->name.'
Email: '.$Person->email.'
Extra Life ID: '.$Person->participantid.'
Specs: '.$Person->specs.'
'.$seatLines.'


Terms: '.$event->terms.'

We look forward to seeing you there!';

		$to = $Person->email;
		$subject = $event->name . ' Registration Confirmation';
		wp_mail($to, $subject, $message);
	}

	public static function sendRemovalEmail($event, $eventseat){
		$edate = date_format(date_create($event->date), 'l F jS\, Y');
		$to = $eventseat->Person->email;
		$subject = $event->name .' Registration Cancelled';
		$message = 'Your registrration for ' . $event->name . ' on ' . $edate . ' has been removed by request or by our team.  

If you think this is a mistake or have questions, please send us a message via our facebook page.

Event Webpage: ' . $event->url . '
Event Details: ' . $event->text . '
';
		wp_mail($to, $subject, $message);
	}
}

class afkContext {
	private $db;
	private $tableEvent;
	private $tableSeatType;
	private $tablePerson;
	private $tableEventSeat;

	public function __construct() {
		global $wpdb;
		$this->db = $wpdb;

		$this->tableEvent = $wpdb->prefix . 'afk_event';
		$this->tableSeatType = $wpdb->prefix . 'afk_seat_type';
		$this->tablePerson = $wpdb->prefix . 'afk_person';
		$this->tableEventSeat = $wpdb->prefix . 'afk_event_seat';
	}

	// events
	public function getEvents() {
		return $this->db->get_results('select * from ' . $this->tableEvent);
	}
	
	public function getEvent($id) {
		$event = new afkEvent();

		$data =  $this->db->get_row('select * from ' . $this->tableEvent . ' where id = ' . $id);
		$event->id = $data->id;
		$event->name = $this->clean($data->name);
		$event->text = $this->clean($data->text);
		$event->date = $data->date;
		$event->url = $data->url;
		$event->backgroundimage = $data->backgroundimage;
		$event->gridwidth = $data->gridwidth;
		$event->gridheight = $data->gridheight;
		$event->adminemail = $data->adminemail;
		$event->active = $data->active;
		$event->minraisedamount = $data->minraisedamount;
		$event->bonusamount = $data->bonusamount;
		$event->bonusamountseats = $data->bonusamountseats;
		$event->terms = $data->terms;
		$event->EventSeats = $this->getEventSeats($id);

		return $event;
	}

	public function getEventPublic($id) {
		$event = $this->getEvent($id);
		$event->adminemail = '';
		foreach ($event->EventSeats as $s) {
			if ($s->personid > 0) {
				$s->Person->email = null;
				$s->Person->specs = null;
			}
		}
		return $event;
	}

	public function addEvent($data) {
		return $this->db->insert(
			$this->tableEvent, 
			array(
				'date' => $data->date,
				'name' => $this->clean($data->name),
				'text' => $this->clean($data->text),
				'url' => $data->url,
				'gridwidth' => $data->gridwidth,
				'gridheight' => $data->gridheight,
				'backgroundimage' => $data->backgroundimage,
				'adminemail' => $data->adminemail,
				'active' => $data->active ? 1:0,
				'minraisedamount' => $data->minraisedamount,
				'bonusamount' => $data->bonusamount,
				'bonusamountseats' => $data->bonusamountseats,
				'terms' => $data->terms
			), 
			array(
				'%s', 
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
				'%d',
				'%s'
			)
		);
	}

	public function updateEvent($data) {
		return $this->db->update(
			$this->tableEvent,
			array(
				'date' => $data->date,
				'name' => $this->clean($data->name),
				'text' => $this->clean($data->text),
				'url' => $data->url,
				'gridwidth' => $data->gridwidth,
				'gridheight' => $data->gridheight,
				'backgroundimage' => $data->backgroundimage,
				'adminemail' => $data->adminemail,
				'active' => $data->active ? 1 : 0,
				'minraisedamount' => $data->minraisedamount,
				'bonusamount' => $data->bonusamount,
				'bonusamountseats' => $data->bonusamountseats,
				'terms' => $data->terms
			), 
			array('id' => $data->id),
			array(
				'%s', 
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
				'%d',
				'%s'
			),
			array('%d')
		);
	}

	public function deleteEvent($event){
		return $this->db->delete($this->tableEvent, array('id' => $event->id));
	}

	// seat types
	public function getSeatTypes() {
		return $this->db->get_results('select * from ' . $this->tableSeatType);
	}

	public function getSeatType($id){
		return $this->db->get_row('select * from ' . $this->tableSeatType . ' where id = ' . $id);
	}

	public function addSeatType($data){
		return $this->db->insert(
			$this->tableSeatType, 
			array(
				'name' => $data->name,
				'color' => $data->color
			), 
			array(
				'%s', 
				'%s'
			)
		);
	}

	public function updateSeatType($data){
		return $this->db->update(
			$this->tableSeatType,
			array(
				'name' => $data->name,
				'color' => $data->color
			), 
			array('id' => $data->id),
			array(
				'%s', 
				'%s'
			),
			array('%d')
		);
	}

	public function deleteSeatType($data){
		return $this->db->delete($this->tableSeatType, array('id' => $data->id));
	}

	// person
	public function getPersons() {
		return $this->db->get_results('select * from ' . $this->tablePerson);
	}

	public function getPerson($id){
		return $this->db->get_row('select * from ' . $this->tablePerson . ' where id = ' . $id);
		if (is_null($data)) {
			return NULL;
		}
		$person = new afkPerson();
		$person->id = $data->id;
		$person->participantid = $data->participantid;
		$person->name = $data->name;
		$person->email = $data->email;
		$person->specs = $this->clean($data->specs);
		return $person;
	}

	public function getPersonByParticipantId($participantid){
		$data = $this->db->get_row('select * from ' . $this->tablePerson . ' where participantid = ' . $participantid);
		if (is_null($data)) {
			return NULL;
		}
		$person = new afkPerson();
		$person->id = $data->id;
		$person->participantid = $data->participantid;
		$person->name = $data->name;
		$person->email = $data->email;
		$person->specs = $this->clean($data->specs);
		return $person;
	}
	public function clean($s){
		$s = str_replace(array("'", "\n", "\r", '"'), '', $s);
		return $s;
	}
	// create person returns person with id or false
	public function addPerson($data){
		$data->specs = $this->clean($data->specs);
		$results = $this->db->insert(
			$this->tablePerson, 
			array(
				'name' => $data->name,
				'email' => $data->email,
				'participantid' => $data->participantid,
				'specs' => $data->specs
			), 
			array(
				'%s', 
				'%s',
				'%d',
				'%s'
			)
		);

		if ($results === false) {
			return false;
		}

		$person = new afkPerson();
		$person->id = $this->db->insert_id;
		$person->participantid = $data->participantid;
		$person->name = $data->name;
		$person->email = $data->email;
		$person->specs = $data->specs;
		return $person;
	}

	public function updatePerson($person){
		// updates a persons name,email, and specs
		$person->specs = $this->clean($person->specs);
		$result = $this->db->update(
			$this->tablePerson, 
			array(
				'name' => $person->name,
				'email' => $person->email,
				'specs' => $person->specs
			), 
			array('id'=>$person->id),
			array(
				'%s', 
				'%s',
				'%s'
			),
			array( '%d' ) 
		);

		if ($result === false){
			return false;
		}
		return $person;
	}

	public function deletePerson($data) {
		//$seat = $this->db->get_results('select * from ' . $this->tableEventSeat . ' where personid = ' . $data->id);
		//$this->removePersonFromSeat($seat);
		return $this->db->delete($this->tablePerson, array('id' => $data->id));
	}
	
	// event seat
	public function getEventSeats($eventid) {
		$data = $this->db->get_results('select es.id, es.eventid, es.positionx, es.positiony, es.seatnumber, ' .
		' es.seattypeid, st.name seattypename, st.color seattypecolor, ' . 
		' es.personid, p.name personname, p.email, p.participantid, p.specs ' .
		' from ' . $this->tableEventSeat . ' es ' .
		' join ' . $this->tableSeatType . ' st on st.id = es.seattypeid ' .
		' left outer join ' . $this->tablePerson . ' p on p.id = es.personid ' .
		' where eventid = ' . $eventid . ' order by positiony, positionx');

		
		$seats = array();
		foreach ($data as $i){
			$seat = new afkEventSeat();
			
			$seat->id = $i->id;
			$seat->eventid = $i->eventid;
			$seat->positionx = $i->positionx;
			$seat->positiony = $i->positiony;
			$seat->seattypeid = $i->seattypeid;
			$seat->personid = $i->personid;
			$seat->seatnumber = $i->seatnumber;

			$seattype = new afkSeatType();
			$seattype->id = $i->seattypeid;
			$seattype->name = $i->seattypename;
			$seattype->color = $i->seattypecolor;
			$seat->SeatType = $seattype;

			if ($seat->personid > 0){
				$person = new afkPerson();
				$person->id = $i->personid;
				$person->name = $i->personname;
				$person->email = $i->email;
				$person->participantid = $i->participantid;
				$person->specs = $this->clean($i->specs);
				$seat->Person = $person;
			}

			array_push($seats, $seat);
		}
		return $seats;
	}



	public function getEventSeat($id){
		$i = $this->db->get_row('select es.id, es.eventid, es.positiony, es.positionx,  es.seattypeid, es.seatnumber, st.name seattypename, st.color seattypecolor, es.personid, p.name personname, p.email, p.participantid, p.specs ' .
		' from ' . $this->tableEventSeat . ' es ' .
		' join ' . $this->tableSeatType . ' st on st.id = es.seattypeid ' .
		' left outer join ' . $this->tablePerson . ' p on p.id = es.personid ' .
		' where es.id = ' . $id);

		

		if (!is_null($i)) {
			$seat = new afkEventSeat();
			
			$seat->id = $i->id;
			$seat->eventid = $i->eventid;
			$seat->positionx = $i->positionx;
			$seat->positiony = $i->positiony;
			$seat->seattypeid = $i->seattypeid;
			$seat->personid = $i->personid;
			$seat->seatnumber = $i->seatnumber;

			$seattype = new afkSeatType();
			$seattype->id = $i->seattypeid;
			$seattype->name = $i->seattypename;
			$seattype->color = $i->seattypecolor;
			$seat->SeatType = $seattype;

			if ($seat->personid > 0){
				$person = new afkPerson();
				$person->id = $i->personid;
				$person->name = $i->personname;
				$person->email = $i->email;
				$person->participantid = $i->participantid;
				$person->specs = $this->clean($i->specs);
				$seat->Person = $person;
			}
			return $seat;
		}
		return $i;
	}

	public function getEventSeatCount($eventid, $positionx, $positiony){
		return $this->db->get_col('select count(*) from ' . $this->tableEventSeat 
		. ' where eventid = ' . $eventid . ' and positionx = ' . $positionx . ' and positiony = ' . $positiony, 0)[0];
	}

	public function addEventSeat($data){
		return $this->db->insert(
			$this->tableEventSeat, 
			array(
				'eventid' => $data->eventid,
				'positionx' => $data->positionx,
				'positiony' => $data->positiony,
				'seattypeid' => $data->seattypeid,
				'seatnumber' => $data->seatnumber
			), 
			array(
				'%d', 
				'%d',
				'%d',
				'%d',
				'%s'
			)
		);
	}

	public function updateEventSeat($data) {
		return $this->db->update(
			$this->tableEventSeat,
			array(
				'seattypeid' => $data->seattypeid,
				'seatnumber' => $data->seatnumber
			), 
			array('id' => $data->id),
			array(
				'%d',
				'%s'
			),
			array('%d')
		);
	}

	public function addPersonToSeat($person, $seat){
		return $this->db->update(
			$this->tableEventSeat,
			array('personid' => $person->id), 
			array('id' => $seat->id),
			array('%d'),
			array('%d')
		);
	}

	// TODO: update all references that may need to use removeFromAllSeats
	public function removePersonFromSeat($seat){
		return $this->db->update(
			$this->tableEventSeat,
			array('personid' => null), 
			array('id' => $seat->id),
			array('%d'),
			array('%d')
		);
	}
	
	public function removePersonFromAllSeats($person){
		return $this->db->update(
			$this->tableEventSeat,
			array('personid' => null), 
			array('personid' => $person->id),
			array('%d'),
			array('%d')
		);
	}

	public function deleteEventSeat($data){
		$seat = $this->getEventSeat($data->id);
		if (isset($seat->personid) && $seat->personid > 0){
			return false;
		}
		return $this->db->delete($this->tableEventSeat, array('id' => $data->id));
	}
}