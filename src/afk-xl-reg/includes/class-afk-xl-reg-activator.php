<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 */


/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 * @author     Your Name <email@example.com>
 */
class AFK_XL_REG_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		AFK_XL_REG_Activator::install();

		
	}

	public static function install() {
		global $wpdb;
		global $afk_db_version;
		global $afk_plugin_version;
		update_option( 'afk_plugin_version', $afk_plugin_version);

		$installed_db_version = get_option("afk_db_version");

		if ($installed_db_version != $afk_db_version) {
-
			$table_event = $wpdb->prefix . 'afk_event';
			$table_seat_type = $wpdb->prefix . 'afk_seat_type';
			$table_person = $wpdb->prefix . 'afk_person';
			$table_event_seat = $wpdb->prefix . 'afk_event_seat';

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $table_event (
				id int NOT NULL AUTO_INCREMENT,
				date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				name varchar(500) NOT NULL,
				text varchar(5000) DEFAULT '' NOT NULL,
				url varchar(250) DEFAULT '' NOT NULL,
				gridwidth int NOT NULL,
				gridheight int NOT NULL,
				backgroundimage varchar(500),
				adminemail varchar(100),
				active boolean DEFAULT 1 NOT NULL,
				minraisedamount int DEFAULT 0 NOT NULL,
				bonusamount int DEFAULT 0 NOT NULL,
				bonusamountseats int DEFAULT 0 NOT NULL,
				terms varchar(2000),
				PRIMARY KEY  (id)
			) $charset_collate;

			CREATE TABLE $table_seat_type (
				id int NOT NULL AUTO_INCREMENT,
				name varchar(150) NOT NULL,
				color varchar(20),
				PRIMARY KEY  (id)
			) $charset_collate;

			CREATE TABLE $table_person (
				id int NOT NULL AUTO_INCREMENT,
				name varchar(100) NOT NULL,
				email varchar(100) NOT NULL,
				participantid int NOT NULL,
				specs varchar(500),
				PRIMARY KEY (id)
			) $charset_collate;

			CREATE TABLE $table_event_seat (
				id int NOT NULL AUTO_INCREMENT,
				eventid int NOT NULL,
				positionx int NOT NULL,
				positiony int NOT NULL,
				seattypeid int NOT NULL,
				personid int,
				seatnumber varchar(20) NOT NULL,
				PRIMARY KEY (id),
				FOREIGN KEY (eventid) REFERENCES $table_event(id),
				FOREIGN KEY (seattypeid) REFERENCES $table_seat_type(id),
				FOREIGN KEY (personid) REFERENCES $table_person(id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			update_option( 'afk_db_version', $afk_db_version );
		}
	}

}
