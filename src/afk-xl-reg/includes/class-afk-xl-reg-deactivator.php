<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    AFK_XL_REG
 * @subpackage AFK_XL_REG/includes
 * @author     Your Name <email@example.com>
 */
class AFK_XL_REG_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		remove_menu_page('afk-admin-home');
		remove_shortcode('xlevent');
	}

}
