<?php
class AFK_XL_REG_UPDATE
{
    /**
     * The plugin current version
     * @var string
     */
    public $current_version;
 
    /**
     * The plugin remote update path
     * @var string
     */
    public $update_path;
	public $update_package;
    /**
     * Plugin Slug (plugin_directory/plugin_file.php)
     * @var string
     */
    public $plugin_slug;
 
    /**
     * Plugin name (plugin_file)
     * @var string
     */
    public $slug;
 
    /**
     * Initialize a new instance of the WordPress Auto-Update class
     * @param string $current_version
     * @param string $update_path
     * @param string $plugin_slug
     */
    function __construct($current_version, $update_path, $update_package, $plugin_slug)
    {
        // Set the class public variables
        $this->current_version = $current_version;
        $this->update_path = $update_path;
		$this->update_package = $update_package;
        $this->plugin_slug = $plugin_slug . '/' . $plugin_slug . '.php';
        $this->slug = $plugin_slug;
 
        // define the alternative API for updating checking
        add_filter('pre_set_site_transient_update_plugins', array(&$this, 'check_update'));
 
        // Define the alternative response for information checking
        add_filter('plugins_api', array(&$this, 'check_info'), 10, 3);
    }
 
    /**
     * Add our self-hosted autoupdate plugin to the filter transient
     *
     * @param $transient
     * @return object $ transient
     */
    public function check_update($transient)
    {
        if (empty($transient->checked)) {
            return $transient;
        }
 
        // Get the remote version
        $remote_version = $this->getRemote_version();
 
        // If a newer version is available, add the update
        if (version_compare($this->current_version, $remote_version, '<')) {
            $obj = new stdClass();
            $obj->slug = $this->slug;
            $obj->new_version = $remote_version;
            $obj->url = $this->update_path;
            $obj->package = $this->update_package;
            $transient->response[$this->plugin_slug] = $obj;
        }
        //var_dump($transient);
        return $transient;
    }
 
    /**
     * Add our self-hosted description to the filter
     *
     * @param boolean $false
     * @param array $action
     * @param object $arg
     * @return bool|object
     */
    public function check_info($false, $action, $arg)
    {
        if ($arg->slug === $this->slug) {
            $information = $this->getRemote_information();
            return $information;
        }
        return false;
    }
 
    /**
     * Return the remote version
     * @return string $remote_version
     */
    public function getRemote_version()
    {
        $request = wp_remote_get($this->update_path);
        if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200) {
            $json = $request['body'];
            $obj = json_decode($json);
            return $obj->version;
        }
        return false;
    }
 
    /**
     * Get information about the remote version
     * @return bool|object
     */
    public function getRemote_information()
    {
        $request = wp_remote_get($this->update_path);
        if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200) {
            $json = $request['body'];
            $jo = json_decode($json);
            $obj = new stdClass();
            $obj->slug = $this->slug;
            $obj->plugin_name = $jo->name;
            $obj->author = $jo->author;
            $obj->version = $jo->version;
            $obj->requires = $jo->requires;
            $obj->tested = $jo->tested;
            $obj->downloaded = 1;
            $obj->last_updated = $jo->last_updated;
            $obj->sections = array(
                'description' => 'Alaska Filmmakers ExtraLife Registration'
            );
            $obj->download_link = $jo->download_link;
            return $obj;
        }
        return false;
    }
 
    /**
     * Return the status of the plugin licensing
     * @return boolean $remote_license
     */
    public function getRemote_license()
    {
        $request = wp_remote_get($this->update_path);
        if (!is_wp_error($request) || wp_remote_retrieve_response_code($request) === 200) {
            $json = $request['body'];
            $obj = json_decode($json);
            return $obj->license;
        }
        return false;
    }
}