const gulp = require("gulp");
const gzip = require("gulp-zip");

gulp.task('dist', () =>
    gulp.src('src/**')
    .pipe(
        gzip('afk-xl-reg.zip')
    ).pipe(gulp.dest('dist'))
);